# Reactive FrankerFaceZ Client
This library provides a client for FFZ with an interface based on functional reactive streams, using Reactor. The
programming model allows for easy streaming of data into your application with non-blocking semantics.

## Adding the Library
Maven:
```xml
<dependencies>
    <dependency>
        <groupId>org.proticity</groupId>
        <artifactId>reactive-ffz-client</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

Gradle:
```groovy
dependencies {
    implementation 'com.proticity.ffz:reactive-ffz-client:0.1.0-SNAPSHOT'
}
```

### Snapshot Releases
To use the snapshot releases of the POM you will need to add the snapshot repository.

Maven:
```xml
<project>
    <repositories>
        <repository>
            <id>oss-snapshots</id>
            <name>Sonatype OSS Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
</project>
```

Gradle:
```groovy
repositories {
    maven {
        url 'https://oss.sonatype.org/content/groups/public'
        mavenContent {
            snapshotsOnly()
        }
    }
}
```

## Documentation
## Documentation
* [Latest JavaDocs](https://proticity.gitlab.io/cloud/reactive-ffz-client/apidocs)
* [Source XRef](https://proticity.gitlab.io/cloud/reactive-ffz-client/source)
* [Unit Test Reports](https://proticity.gitlab.io/cloud/reactive-ffz-client/unit-tests)
* [Integration Test Reports](https://proticity.gitlab.io/cloud/reactive-ffz-client/integration-tests)
* [Test Code Coverage](https://proticity.gitlab.io/cloud/reactive-ffz-client/coverage)
* [SonarQube Analysis](https://sonarcloud.io/dashboard?id=org.proticity%3Areactive-ffz-client)

### Creating a Client
```java
FfzClient client = FfzClient.create().connect();
```

Some options are available for advanced uses.
```java
FfzClient client = FfzClient.create()
    .endpoint("http://localhost:8080/api") // Override the API endpoint
    .secure(false) // Force off HTTPS
    .objectMapper(new ObjectMapper()) // Override the Jackson ObjectMapper
    .connectionProvider(HttpRequests.get()) // Override Reactor-Netty HTTP connection provider
    .sslProviderBuilder(provider) // Override the SSL Provider Builder for HTTPS connections
    .connect();
```

### Channels
Channel information can be retrieved by the integer ID of the channel (i.e. the user who owns it) on Twitch or by the
Twitch username.

```java
Channel channel = client.channel("TwitchPresents").block();
System.out.println("Channel: " + channel.getName());
System.out.println("Twitch ID: " + channel.getTwitchId());
System.out.println("Emotes:");
for (Optional<EmoteSet> set : channel.getEmoteSets().values()) {
    if (set.isEmpty()) {
        continue;
    }
    for (Emote emote : set.getEmotes()) {
        System.out.println("\t" + emote.getName());
    }
}
```

The badges and emote sets for a channel are fully included by default via a map of their IDs to the badge or emote set
details. The keys of this map are always included and can be used to iterate over the IDs of the objects, but it is
optional to include the details. The optional second argument determines if the details are included.

```java
Channel channel = client.channel(132465, false).block();
System.out.println("Channel: " + channel.getName());
System.out.println("Emote Set IDs:");
// The values of this map are Optional.empty();
for (int emoteSetId : channel.getEmoteSet().keys()) {
    System.out.println("\t" + emoteSetId);
}
```

### Emote Sets
Global emote sets can be streamed from the client by calling `globalEmoteSets()`. Note that global emote sets serve both
as sets everyone can use and also a limited number of personal emote sets associated with only specific users (similar
to BTTV personal emotes, but not generally available to regular users). The `EmoteSet#getPermittedUsers()` method can
determine if only some users are permitted to use the set, and returns `Optional.empty()` if there are no restrictions.

```java
// List all emotes in unrestricted emote sets.
client.globalEmoteSets()
    .filter(set -> set.getPermittedUsers().isEmpty())
    .flatMap(EmoteSet::getEmotes)
    .map(Emote::getName)
    .subscribe(System.out::println);

// Create a map of usernames to their restricted emote sets
Map<String, Collection<EmoteSet>> emoteSetAssignments = client.globalEmoteSets()
    .filter(set -> set.getPermittedUsers().isPresent())
    .handle((set, sink) -> {
        for (var username : set.getPermittedUsers()) {
            sink.next(Tuples.of(username, set));
        }
    }).collectMultimap(Tuple2::getT1)
    .block();
```

Any emote set, both global and channel emote sets, can be accessed by their integer ID if it is known.

```java
EmoteSet set = client.emoteSet(4330).block();
```

### Emote Search
FFZ provides a search API for finding emotes across the entire catalog of emotes, with results being returned in pages.
The reactive API provided by this client allows results to be streamed in as a `Flux`. For large queries which may take
time it is possible to stream the number of total matching emotes at each page in the result, as this number may change
from the first page of results to the end.

```java
// Create a search params for emotes with "RIP" in the name, up to 1000 maximum.
// Note that the max results is not part of the search, it merely ends the stream early.
// Therefore the total matching result count can be greater.
EmoteSearch params = EmoteSearch.forAll().nameFilter("RIP").maxResults(1000);

// Perform the search.
ConnectableEmoteResults search = client.emotes(params);

// Print each emote name, and at the end print the final number of total matching emotes.
search.results().map(Emote::getName).subscribe(System.out::println);
search.totalResults().last().subscribe(System.out::println)
search.connect();
```

The results object returned acts as a handle to the result streams. It behaves similarly to a `ConnectableFlux`, in that
`ConnectableEmoteResult#connect()` must be invoked to begin getting results after subscribers are added to the result
streams for emotes and/or total matching counts. `EmoteResult#autoConnect()` will return an `EmoteResult` which begins
streaming results upon subscription immediately.

```java
EmoteSearch params = EmoteSearch.forAll().nameFilter("RIP").maxResults(1000);

// Print the name of all returned emotes with automatic connection.
client.emotes(params)
    .autoConnect()
    .results()
    .map(Emote::getName)
    .subscribe(System.out::println);
```

### Badges
The full set of badges and badge assignments can be retrieved in a single call. Optionally, the badge assignments may be
excluded, in which case only the full set of badges in the system are returned. The badge collection is returned as a
map keyed by their integer ID, and assignments are map of usernames to a submap of badges which are assigned to them. If
assignments are not included the assignment map is set to `Optional.empty()`.

```java
// Include assignments.
Badges badges = client.badges(true).block();
for (Badge badge : badges.getBadges().values()) {
    System.out.println(badge.getName());
}

// List badge titles assigned to each user.
for (Map.Entry<String, Map<Integer, Badge>> assignment : badges.getAssignments().get().entries()) {
    System.out.println(assignment.getKey() + ":");
    for (Badge badge : assignment.getValue().values()) {
        System.out.println("\t" + badge.getTitle());
    }
    System.out.println();
}
```

Individual badges can be retrieved by their integer ID or name, and may or may not include the set of users to which
they have been assigned. Assignments are included by default.

```java
// List users with the bot badge.
var botBadge = client.badge("bot").block();
for (var username : botBadge.getAssignments().get()) {
    System.out.println(username);
}

// Get the dev badge by ID, without assignments.
var devBadge = client.badge(1, false).block();
assert devBadge.getAssignments().isEmpty();
```

### Users
User information is available by the user's Twitch username or unique Twitch integer ID. Badges and emote sets for the
user are included by default but can be excluded if desired. When excluded, the IDs are still included but details are
not.

```java
// Get user and explicitly include all badges and emote sets.
var nightbot = client.user("nightbot", true).block();
System.out.println("Twitch ID: " + nightbot.getTwitchId());
```
