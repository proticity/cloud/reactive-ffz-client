package org.proticity.ffz.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FfzClientTest {
    @Test
    public void testDisposeNow() {
        Exception error = null;
        var client = FfzClient.create().connect();
        try {
            client.disposeNow();
        } catch (final Exception e) {
            error = e;
        }
        Assertions.assertNull(error);
    }
}
