package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;

public class UserTest {
    private static final String TEST_USER_JSON = "{\"badges\":{\"1\":{\"color\":\"#FAAF19\",\"css\":null,\"id\":1," +
            "\"image\":\"//cdn.frankerfacez.com/badge/1/1\",\"name\":\"developer\",\"replaces\":null,\"slot\":5," +
            "\"title\":\"FFZ Developer\",\"urls\":{\"1\":\"//cdn.frankerfacez.com/badge/1/1\",\"2\":" +
            "\"//cdn.frankerfacez.com/badge/1/2\",\"4\":\"//cdn.frankerfacez.com/badge/1/4\"}}},\"sets\":" +
            "{\"4330\":{\"_type\":0,\"css\":null,\"description\":null,\"emoticons\":[{\"css\":null,\"height\":32," +
            "\"hidden\":false,\"id\":147400,\"margins\":null,\"modifier\":false,\"name\":\"OBOY\",\"offset\":null," +
            "\"owner\":{\"_id\":2600,\"display_name\":\"Drast\",\"name\":\"drast\"},\"public\":true,\"urls\":" +
            "{\"1\":\"//cdn.frankerfacez.com/2c54ee3f057f38a3205990d1bc9f1773.PNG\",\"2\":" +
            "\"//cdn.frankerfacez.com/1f78c913e9d774dc516a34c31365a264.PNG\",\"4\":" +
            "\"//cdn.frankerfacez.com/a828d1810660377d6966c7d332a148c2.png\"},\"width\":27},{\"css\":null," +
            "\"height\":32,\"hidden\":false,\"id\":143930,\"margins\":null,\"modifier\":false,\"name\":\"OiMinna\"," +
            "\"offset\":null,\"owner\":{\"_id\":2,\"display_name\":\"dansalvato\",\"name\":\"dansalvato\"}," +
            "\"public\":true,\"urls\":{\"1\":\"//cdn.frankerfacez.com/ff7001713323e7a04461e40a66f971a2.PNG\",\"2\":" +
            "\"//cdn.frankerfacez.com/3f5b1ef8c2a33f1dc1a5b726c5be53ac.PNG\",\"4\":" +
            "\"//cdn.frankerfacez.com/193e59bbfef68b8f7172db49137e6f85.png\"},\"width\":32},{\"css\":null," +
            "\"height\":11,\"hidden\":false,\"id\":24999,\"margins\":null,\"modifier\":false,\"name\":\"AndKnuckles\"," +
            "\"offset\":null,\"owner\":{\"_id\":1,\"display_name\":\"SirStendec\",\"name\":\"sirstendec\"}," +
            "\"public\":true,\"urls\":{\"1\":\"//cdn.frankerfacez.com/2104d535159a04a63d479adcbc236600.PNG\",\"2\":" +
            "\"//cdn.frankerfacez.com/f8457911a8931e9839e7c9ae1870e069.PNG\",\"4\":" +
            "\"//cdn.frankerfacez.com/d16e1c9fd592e1e5ac64a7a3406172c7.png\"},\"width\":80}],\"icon\":null," +
            "\"id\":4330,\"title\":\": Sten's Cheaty Emotes\"}},\"user\":{\"avatar\":" +
            "\"https://static-cdn.jtvnw.net/jtv_user_pictures/sirstendec-profile_image-1514bdb9d03a0954-300x300.png\"," +
            "\"badges\":[1],\"display_name\":\"SirStendec\",\"emote_sets\":[4330],\"id\":1,\"is_donor\":true,\"name\":" +
            "\"sirstendec\",\"twitch_id\":49399878}}";
    private static final String TEST_USER_ABRIDGED_JSON = "{\"user\":{\"avatar\":" +
            "\"https://static-cdn.jtvnw.net/jtv_user_pictures/sirstendec-profile_image-1514bdb9d03a0954-300x300.png\"," +
            "\"badges\":[1],\"display_name\":\"SirStendec\",\"emote_sets\":[4330],\"id\":1,\"is_donor\":true,\"name\":" +
            "\"sirstendec\",\"twitch_id\":49399878}}";

    private DisposableServer server;
    private FfzClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testUserById() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/user/id/1", req.uri());
                    return res.sendString(Mono.just(TEST_USER_JSON));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var user = client.user(1).block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertEquals(1, user.getBadges().size());
        Assertions.assertTrue(user.getBadges().get(1).isPresent());
        Assertions.assertEquals(1, user.getEmoteSets().size());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isPresent());
    }

    @Test
    public void testUserByName() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/user/sirstendec", req.uri());
                    return res.sendString(Mono.just(TEST_USER_JSON));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var user = client.user("sirstendec").block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertEquals(1, user.getBadges().size());
        Assertions.assertTrue(user.getBadges().get(1).isPresent());
        Assertions.assertEquals(1, user.getEmoteSets().size());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isPresent());
    }

    @Test
    public void testUserByIdNoEmoteSets() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/_user/id/1", req.uri());
                    return res.sendString(Mono.just(TEST_USER_ABRIDGED_JSON));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var user = client.user(1, false).block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertEquals(1, user.getBadges().size());
        Assertions.assertTrue(user.getBadges().get(1).isEmpty());
        Assertions.assertEquals(1, user.getEmoteSets().size());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isEmpty());
    }

    @Test
    public void testUserByNameNoEmoteSets() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/_user/sirstendec", req.uri());
                    return res.sendString(Mono.just(TEST_USER_ABRIDGED_JSON));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var user = client.user("sirstendec", false).block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertEquals(1, user.getBadges().size());
        Assertions.assertTrue(user.getBadges().get(1).isEmpty());
        Assertions.assertEquals(1, user.getEmoteSets().size());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isEmpty());
    }

    @Test
    public void testUserSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/user/sirstendec", req.uri());
                    return res.sendString(Mono.just(TEST_USER_JSON));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var originalUser = client.user("sirstendec").block();
        Assertions.assertNotNull(originalUser);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalUser);
        var user = mapper.readValue(serialized, User.class);
        Assertions.assertEquals(originalUser, user);
        Assertions.assertEquals(originalUser.hashCode(), user.hashCode());

        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertEquals(1, user.getBadges().size());
        Assertions.assertTrue(user.getBadges().get(1).isPresent());
        Assertions.assertEquals(1, user.getEmoteSets().size());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isPresent());
    }

    @Test
    public void testUserNotFound() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/user/bob", req.uri());
                    return res.status(404).sendString(Mono.just("{}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.user("bob").block());
    }

    @Test
    public void testUserBadResponse() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/user/id/123", req.uri());
                    return res.status(404).sendString(Mono.just("{foo}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.user(123).block());
    }
}
