package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;
import java.util.HashSet;

public class BadgeTest {
    private DisposableServer server;
    private FfzClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testBadgeById() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badge/2", req.uri());
                    return res.sendString(Mono.just("{\"badge\":{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                            "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                            "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                            "\"getCss\":null},\"users\":{\"2\":[\"sirstendec\",\"dansalvato\"]}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badge = client.badge(2).block();
        Assertions.assertNotNull(badge);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertTrue(badge.getCss().isEmpty());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertEquals(2, badge.getAssignments().get().size());
        Assertions.assertTrue(badge.getAssignments().get().contains("dansalvato"));
        Assertions.assertTrue(badge.getAssignments().get().contains("sirstendec"));
        Assertions.assertEquals("moderator", badge.getReplaces().get());
    }

    @Test
    public void testBadgeByName() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badge/bot", req.uri());
                    return res.sendString(Mono.just("{\"badge\":{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                            "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                            "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                            "\"getCss\":null},\"users\":{\"2\":[\"sirstendec\",\"dansalvato\"]}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badge = client.badge("bot").block();
        Assertions.assertNotNull(badge);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertEquals(2, badge.getAssignments().get().size());
        Assertions.assertTrue(badge.getAssignments().get().contains("dansalvato"));
        Assertions.assertTrue(badge.getAssignments().get().contains("sirstendec"));
        Assertions.assertEquals("moderator", badge.getReplaces().get());
    }

    @Test
    public void testBadgeNoAssignments() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/_badge/2", req.uri());
                    return res.sendString(Mono.just("{\"badge\":{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                            "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                            "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                            "\"getCss\":null}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badge = client.badge(2, false).block();
        Assertions.assertNotNull(badge);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertTrue(badge.getAssignments().isEmpty());
        Assertions.assertEquals("moderator", badge.getReplaces().get());
    }

    @Test
    public void testBadgeNotFound() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badge/2", req.uri());
                    return res.status(404).sendString(Mono.just("{}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.badge(2).block());
    }

    @Test
    public void testBadBadgeResponse() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badge/2", req.uri());
                    return res.sendString(Mono.just("{\"badge\":{}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.badge(2).block());
    }

    @Test
    public void testBadges() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badges", req.uri());
                        return res.sendString(Mono.just("{\"badges\":[{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                                "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                                "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                                "\"getCss\":null},{\"id\":1,\"name\":\"developer\",\"title\":\"FFZ Developer\",\"slot\":5," +
                                "\"replaces\":null,\"color\":\"#FAAF19\",\"image\":\"dev-badge-1.png\",\"urls\":" +
                                "{\"1\":\"dev-badge-1.png\",\"2\":\"dev-badge-2.png\",\"4\":\"dev-badge-4.png\"}," +
                                "\"getCss\":null}],\"users\":{\"1\":[\"sirstendec\",\"dansalvato\"],\"3\":[\"sirstendec\"]}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badgeInfo = client.badges().block();
        Assertions.assertNotNull(badgeInfo);

        var badge = badgeInfo.getBadges().get(2);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertEquals(0, badge.getAssignments().get().size());
        Assertions.assertEquals("moderator", badge.getReplaces().get());

        badge = badgeInfo.getBadges().get(1);
        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("#FAAF19", badge.getColor().get());
        Assertions.assertEquals(5, badge.getSlot());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getReplaces().isEmpty());
        Assertions.assertEquals(2, badge.getAssignments().get().size());
        Assertions.assertTrue(badge.getAssignments().get().contains("dansalvato"));
        Assertions.assertTrue(badge.getAssignments().get().contains("sirstendec"));

        Assertions.assertTrue(badgeInfo.getBadgeAssignments().isPresent());
        Assertions.assertEquals(2, badgeInfo.getBadgeAssignments().get().size());
        var assignments = badgeInfo.getBadgeAssignments().get().get("sirstendec");
        Assertions.assertNotNull(assignments);
        Assertions.assertEquals(1, assignments.size());
        Assertions.assertSame(badge, assignments.get(1));
        assignments = badgeInfo.getBadgeAssignments().get().get("dansalvato");
        Assertions.assertNotNull(assignments);
        Assertions.assertEquals(1, assignments.size());
        Assertions.assertSame(badge, assignments.get(1));
    }

    @Test
    public void testSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/badges", req.uri());
                    return res.sendString(Mono.just("{\"badges\":[{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                            "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                            "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                            "\"getCss\":null},{\"id\":1,\"name\":\"developer\",\"title\":\"FFZ Developer\",\"slot\":5," +
                            "\"replaces\":null,\"color\":\"#FAAF19\",\"image\":\"dev-badge-1.png\",\"urls\":" +
                            "{\"1\":\"dev-badge-1.png\",\"2\":\"dev-badge-2.png\",\"4\":\"dev-badge-4.png\"}," +
                            "\"getCss\":null}],\"users\":{\"1\":[\"sirstendec\",\"dansalvato\"],\"3\":[\"sirstendec\"]}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var originalBadgeInfo = client.badges().block();
        Assertions.assertNotNull(originalBadgeInfo);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalBadgeInfo);
        var badgeInfo = mapper.readValue(serialized, Badges.class);
        Assertions.assertEquals(originalBadgeInfo, badgeInfo);

        var badge = badgeInfo.getBadges().get(2);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertEquals(0, badge.getAssignments().get().size());
        Assertions.assertEquals("moderator", badge.getReplaces().get());

        badge = badgeInfo.getBadges().get(1);
        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("#FAAF19", badge.getColor().get());
        Assertions.assertEquals(5, badge.getSlot());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getReplaces().isEmpty());
        Assertions.assertEquals(2, badge.getAssignments().get().size());
        Assertions.assertTrue(badge.getAssignments().get().contains("dansalvato"));
        Assertions.assertTrue(badge.getAssignments().get().contains("sirstendec"));

        Assertions.assertTrue(badgeInfo.getBadgeAssignments().isPresent());
        Assertions.assertEquals(2, badgeInfo.getBadgeAssignments().get().size());
        var assignments = badgeInfo.getBadgeAssignments().get().get("sirstendec");
        Assertions.assertNotNull(assignments);
        Assertions.assertEquals(1, assignments.size());
        Assertions.assertSame(badge, assignments.get(1));
        assignments = badgeInfo.getBadgeAssignments().get().get("dansalvato");
        Assertions.assertNotNull(assignments);
        Assertions.assertEquals(1, assignments.size());
        Assertions.assertSame(badge, assignments.get(1));

        // Test identity.
        var set = new HashSet<Badges>();
        set.add(originalBadgeInfo);
        Assertions.assertTrue(set.contains(badgeInfo));
    }

    @Test
    public void testBadgesNoAssignments() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                        Assertions.assertEquals("/_badges", req.uri());
                        return res.sendString(Mono.just("{\"badges\":[{\"id\":2,\"name\":\"bot\",\"title\":\"Bot\"," +
                                "\"slot\":1,\"replaces\":\"moderator\",\"color\":\"#595959\",\"image\":\"bot-badge-1.png\"," +
                                "\"urls\":{\"1\":\"bot-badge-1.png\",\"2\":\"bot-badge-2.png\",\"4\":\"bot-badge-4.png\"}," +
                                "\"getCss\":null},{\"id\":1,\"name\":\"developer\",\"title\":\"FFZ Developer\",\"slot\":5," +
                                "\"replaces\":null,\"color\":\"#FAAF19\",\"image\":\"dev-badge-1.png\",\"urls\":" +
                                "{\"1\":\"dev-badge-1.png\",\"2\":\"dev-badge-2.png\",\"4\":\"dev-badge-4.png\"}," +
                                "\"getCss\":null}]}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badgeInfo = client.badges(false).block();
        Assertions.assertNotNull(badgeInfo);

        var badge = badgeInfo.getBadges().get(2);
        Assertions.assertEquals(2, badge.getFfzId());
        Assertions.assertEquals("bot", badge.getName());
        Assertions.assertEquals("#595959", badge.getColor().get());
        Assertions.assertEquals(1, badge.getSlot());
        Assertions.assertEquals("Bot", badge.getTitle());
        Assertions.assertEquals("moderator", badge.getReplaces().get());
        Assertions.assertTrue(badge.getAssignments().isEmpty());

        badge = badgeInfo.getBadges().get(1);
        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("#FAAF19", badge.getColor().get());
        Assertions.assertEquals(5, badge.getSlot());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getReplaces().isEmpty());
        Assertions.assertTrue(badge.getAssignments().isEmpty());

        Assertions.assertTrue(badgeInfo.getBadgeAssignments().isEmpty());
    }
}
