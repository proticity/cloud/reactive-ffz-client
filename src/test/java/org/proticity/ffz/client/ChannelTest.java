package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;

public class ChannelTest {
    private DisposableServer server;
    private FfzClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testChannelById() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/room/id/70902416", req.uri());
                    return res.sendString(Mono.just("{\"room\":{\"_id\":235625,\"twitch_id\":70902416,\"id\":" +
                            "\"charmedbaryon\",\"is_group\":false,\"display_name\":\"CharmedBaryon\",\"set\":235637," +
                            "\"moderator_badge\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"mod_urls\":{\"1\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"2\":null,\"4\":null},\"user_badges\":{},\"css\":null},\"sets\":{\"235637\":" +
                            "{\"id\":235637,\"_type\":1,\"icon\":null,\"title\":\"Channel: CharmedBaryon\"," +
                            "\"css\":null,\"emoticons\":[{\"id\":38879,\"name\":\"Klappa\",\"height\":29,\"width\":32," +
                            "\"public\":true,\"hidden\":false,\"modifier\":false,\"offset\":null,\"margins\":null," +
                            "\"css\":null,\"owner\":{\"_id\":1537,\"name\":\"tyriansr\",\"display_name\":\"TyrianSR\"}," +
                            "\"urls\":{\"1\":\"https://cdn.frankerfacez.com/3217bad4f8d4b6cb96785942237b7af7.PNG\"," +
                            "\"2\":\"https://cdn.frankerfacez.com/9f73464b461d9af1bc2c5b43f6723fc4.PNG\",\"4\":" +
                            "\"https://cdn.frankerfacez.com/bd76029d57ad6af5815a3aa9784c8c11.png\"}}]}}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var channel = client.channel(70902416).block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertEquals(235625, channel.getFfzId());
        Assertions.assertEquals(70902416, channel.getTwitchId());
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertEquals(1, channel.getEmoteSets().size());
        Assertions.assertNotNull(channel.getEmoteSets().get(235637));
    }

    @Test
    public void testChannelByName() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/room/charmedbaryon", req.uri());
                    return res.sendString(Mono.just("{\"room\":{\"_id\":235625,\"twitch_id\":70902416,\"id\":" +
                            "\"charmedbaryon\",\"is_group\":false,\"display_name\":\"CharmedBaryon\",\"set\":235637," +
                            "\"moderator_badge\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"mod_urls\":{\"1\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"2\":null,\"4\":null},\"user_badges\":{},\"css\":null},\"sets\":{\"235637\":" +
                            "{\"id\":235637,\"_type\":1,\"icon\":null,\"title\":\"Channel: CharmedBaryon\"," +
                            "\"css\":null,\"emoticons\":[{\"id\":38879,\"name\":\"Klappa\",\"height\":29,\"width\":32," +
                            "\"public\":true,\"hidden\":false,\"modifier\":false,\"offset\":null,\"margins\":null," +
                            "\"css\":null,\"owner\":{\"_id\":1537,\"name\":\"tyriansr\",\"display_name\":\"TyrianSR\"}," +
                            "\"urls\":{\"1\":\"https://cdn.frankerfacez.com/3217bad4f8d4b6cb96785942237b7af7.PNG\"," +
                            "\"2\":\"https://cdn.frankerfacez.com/9f73464b461d9af1bc2c5b43f6723fc4.PNG\",\"4\":" +
                            "\"https://cdn.frankerfacez.com/bd76029d57ad6af5815a3aa9784c8c11.png\"}}]}}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var channel = client.channel("charmedbaryon").block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertEquals(235625, channel.getFfzId());
        Assertions.assertEquals(70902416, channel.getTwitchId());
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertEquals(1, channel.getEmoteSets().size());
        Assertions.assertNotNull(channel.getEmoteSets().get(235637));
    }

    @Test
    public void testChannelByIdNoEmoteSets() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/_room/id/70902416", req.uri());
                    return res.sendString(Mono.just("{\"room\":{\"_id\":235625,\"twitch_id\":70902416,\"id\":" +
                            "\"charmedbaryon\",\"is_group\":false,\"display_name\":\"CharmedBaryon\",\"set\":235637," +
                            "\"moderator_badge\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"mod_urls\":{\"1\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"2\":null,\"4\":null},\"user_badges\":{},\"css\":null}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var channel = client.channel(70902416, false).block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertEquals(235625, channel.getFfzId());
        Assertions.assertEquals(70902416, channel.getTwitchId());
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertEquals(1, channel.getEmoteSets().size());
        Assertions.assertTrue(channel.getEmoteSets().get(235637).isEmpty());
    }

    @Test
    public void testChannelByNameNoEmoteSets() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/_room/charmedbaryon", req.uri());
                    return res.sendString(Mono.just("{\"room\":{\"_id\":235625,\"twitch_id\":70902416,\"id\":" +
                            "\"charmedbaryon\",\"is_group\":false,\"display_name\":\"CharmedBaryon\",\"set\":235637," +
                            "\"moderator_badge\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"mod_urls\":{\"1\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"2\":null,\"4\":null},\"user_badges\":{},\"css\":null}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var channel = client.channel("charmedbaryon", false).block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertEquals(235625, channel.getFfzId());
        Assertions.assertEquals(70902416, channel.getTwitchId());
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertEquals(1, channel.getEmoteSets().size());
        Assertions.assertTrue(channel.getEmoteSets().get(235637).isEmpty());
    }

    @Test
    public void testChannelSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/room/charmedbaryon", req.uri());
                    return res.sendString(Mono.just("{\"room\":{\"_id\":235625,\"twitch_id\":70902416,\"id\":" +
                            "\"charmedbaryon\",\"is_group\":false,\"display_name\":\"CharmedBaryon\",\"set\":235637," +
                            "\"moderator_badge\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"mod_urls\":{\"1\":\"//cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1\"," +
                            "\"2\":null,\"4\":null},\"user_badges\":{},\"css\":null},\"sets\":{\"235637\":" +
                            "{\"id\":235637,\"_type\":1,\"icon\":null,\"title\":\"Channel: CharmedBaryon\"," +
                            "\"css\":null,\"emoticons\":[{\"id\":38879,\"name\":\"Klappa\",\"height\":29,\"width\":32," +
                            "\"public\":true,\"hidden\":false,\"modifier\":false,\"offset\":null,\"margins\":null," +
                            "\"css\":null,\"owner\":{\"_id\":1537,\"name\":\"tyriansr\",\"display_name\":\"TyrianSR\"}," +
                            "\"urls\":{\"1\":\"https://cdn.frankerfacez.com/3217bad4f8d4b6cb96785942237b7af7.PNG\"," +
                            "\"2\":\"https://cdn.frankerfacez.com/9f73464b461d9af1bc2c5b43f6723fc4.PNG\",\"4\":" +
                            "\"https://cdn.frankerfacez.com/bd76029d57ad6af5815a3aa9784c8c11.png\"}}]}}}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var originalChannel = client.channel("charmedbaryon").block();
        Assertions.assertNotNull(originalChannel);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalChannel);
        var channel = mapper.readValue(serialized, Channel.class);
        Assertions.assertEquals(originalChannel, channel);
        Assertions.assertEquals(originalChannel.hashCode(), channel.hashCode());

        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertEquals(235625, channel.getFfzId());
        Assertions.assertEquals(70902416, channel.getTwitchId());
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertEquals(1, channel.getEmoteSets().size());
        Assertions.assertNotNull(channel.getEmoteSets().get(235637));

        Assertions.assertEquals("https://cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1",
                channel.getModeratorBadge().get().getUrl(1));
        Assertions.assertEquals("https://cdn.frankerfacez.com/room-badge/mod/charmedbaryon/1",
                channel.getModeratorBadge().get().getUrl(2));
        Assertions.assertThrows(IllegalArgumentException.class, () ->channel.getModeratorBadge().get().getUrl(0));
    }

    @Test
    public void testChannelNotFound() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/room/chan", req.uri());
                    return res.status(404).sendString(Mono.just("{}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.channel("chan").block());
    }

    @Test
    public void testChannelBadResponse() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> {
                    Assertions.assertEquals("/room/id/123", req.uri());
                    return res.status(404).sendString(Mono.just("{foo}"));
                }).bindNow();

        client = FfzClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(FfzApiException.class, () -> client.channel(123).block());
    }
}
