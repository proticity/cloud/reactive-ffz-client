/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Serializable;
import java.time.Duration;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A specification for the parameters of listing all emotes.
 */
@ParametersAreNonnullByDefault
public class EmoteSearch implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The default timeout period to use for fetching each page.
     */
    private static final Duration DEFAULT_PAGE_TIMEOUT = Duration.ofSeconds(60);

    /**
     * The default size of a page of emote results.
     */
    private static final int DEFAULT_PAGE_SIZE = 200;

    /**
     * The name filter to apply on emote names.
     */
    String nameFilter;

    /**
     * The maximum number of emotes per page to fetch.
     */
    int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * Whether to limit the results to only emotes with high-DPI image options.
     */
    boolean highDpiRequired;

    /**
     * The page to retrieve, if getting only a single page.
     */
    Integer page;

    /**
     * Whether this search will only return public emotes or also return private ones.
     */
    boolean publicOnly = true;

    /**
     * The sorted order in which results should be streamed back.
     */
    SortOrder sortOrder = SortOrder.CREATED_DESC;

    /**
     * The maximum number of results that will streamed back, after which the stream will stop even if there are more
     * matching results.
     */
    int maxResults = Integer.MAX_VALUE;

    /**
     * The timeout period to wait for each separate page of results.
     */
    Duration pageTimeout = DEFAULT_PAGE_TIMEOUT;

    /**
     * Construct a new {@link EmoteSearch}.
     */
    EmoteSearch() {
    }

    /**
     * Returns a new {@link EmoteSearch} for returning a single specific page of results.
     *
     * <p>
     * This will skip to the given page and only stream results from that page. Each page of the given page size before
     * the requested page will be skipped over.
     * </p>
     *
     * @param pageSize the number of results to return in the page.
     * @param page the number of the page to return.
     *
     * @return A new {@link EmoteSearch}.
     */
    public static EmoteSearch forPage(int pageSize, int page) {
        return new EmoteSearch().pageSize(pageSize).page(page);
    }

    /**
     * Returns a search for all emotes.
     *
     * @return A new {@link EmoteSearch}.
     */
    public static EmoteSearch forAll() {
        return forAll(DEFAULT_PAGE_SIZE);
    }

    /**
     * Returns a search for all emotes.
     *
     * @param pageSize the size of each page that will be returned for each batch of results.
     *
     * @return A new {@link EmoteSearch}.
     */
    public static EmoteSearch forAll(int pageSize) {
        return new EmoteSearch().pageSize(pageSize);
    }

    /**
     * Returns a new search for a given number of emotes.
     *
     * @param maxResults the maximum number of emotes that will be returned.
     *
     * @return A new {@link EmoteSearch}.
     */
    public static EmoteSearch forMax(int maxResults) {
        return forMax(maxResults, DEFAULT_PAGE_SIZE);
    }

    /**
     * Returns a new search for a given number of emotes.
     *
     * @param maxResults the maximum number of emotes that will be returned.
     * @param pageSize the size of each batch that will be returned from FFZ.
     *
     * @return A new {@link EmoteSearch}.
     */
    public static EmoteSearch forMax(int maxResults, int pageSize) {
        return new EmoteSearch().maxResults(maxResults).pageSize(pageSize);
    }

    /**
     * Set name filter to apply on emote names.
     *
     * @param nameFilter name filter to apply on emote names.
     *
     * @return A reference to this {@link EmoteSearch}.
     */
    public EmoteSearch nameFilter(@Nullable String nameFilter) {
        this.nameFilter = nameFilter;
        return this;
    }

    /**
     * Set the number of emotes to return per page.
     *
     * @param pageSize the number of emotes to return per page.
     *
     * @return This {@link EmoteSearch}.
     */
    private EmoteSearch pageSize(int pageSize) {
        if (pageSize < 1) {
            throw new IllegalArgumentException("Page size must be greater than zero.");
        }
        this.pageSize = Math.min(DEFAULT_PAGE_SIZE, pageSize);
        return this;
    }

    /**
     * Sets whether to limit the results to only emotes with high-DPI image options.
     *
     * @param highDpiRequired whether to limit the results to only emotes with high-DPI image options.
     *
     * @return A reference to this {@link EmoteSearch}.
     */
    public EmoteSearch highDpiRequired(boolean highDpiRequired) {
        this.highDpiRequired = highDpiRequired;
        return this;
    }

    /**
     * Sets the specific page to retrieve, if getting just one page.
     *
     * @param page the page to retrieve.
     *
     * @return A reference to this {@link EmoteSearch}.
     */
    protected EmoteSearch page(@Nullable Integer page) {
        if (page != null && page < 1) {
            throw new IllegalArgumentException("Page number must be null or greater than zero.");
        }
        this.page = page;
        return this;
    }

    /**
     * Set whether this search will only return public emotes or also return private ones.
     *
     * @param publicOnly whether this search will only return public emotes or also return private ones.
     *
     * @return A reference to this {@link EmoteSearch}.
     */
    public EmoteSearch publicOnly(boolean publicOnly) {
        this.publicOnly = publicOnly;
        return this;
    }

    /**
     * Sets the sorted order in which results should be streamed back.
     *
     * @param sortOrder the sorted order in which results should be streamed back.
     *
     * @return A reference to this {@link EmoteSearch}.
     */
    public EmoteSearch sortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
        return this;
    }

    /**
     * Sets the maximum number of results that will streamed back, after which the stream will stop even if there are
     * more matching results.
     *
     * @param maxResults the maximum number of results to return in the stream.
     *
     * @return This {@link EmoteSearch}.
     */
    protected EmoteSearch maxResults(int maxResults) {
        if (maxResults < 1) {
            throw new IllegalArgumentException("Max results must be greater than zero.");
        }
        this.maxResults = maxResults;
        return this;
    }

    /**
     * Sets the timeout period to wait for each separate page of results.
     *
     * @param pageTimeout the timeout period to wait for each separate page of results.
     *
     * @return This {@link EmoteSearch}.
     */
    public EmoteSearch pageTimeout(Duration pageTimeout) {
        this.pageTimeout = pageTimeout;
        return this;
    }
}
