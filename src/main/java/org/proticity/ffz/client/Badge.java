/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * Describes an FFZ user badge.
 *
 * @see ModeratorBadge
 * @see Badges
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Badge implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A type reference for a tree of URLs.
     */
    private static final TypeReference<TreeMap<Integer, String>> URLS_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * The unique of the badge in FFZ.
     */
    private int ffzId;

    /**
     * A unique name string to identify the badge.
     */
    private String name;

    /**
     * A friendly title for the badge.
     */
    private String title;

    /**
     * The slot the badge appears in, to imply an ordering.
     */
    private int slot;

    /**
     * A CSS style to apply to the badge, if any.
     */
    private String css;

    /**
     * The background color for the badge, if any.
     */
    private String color;

    /**
     * The name of a standard Twitch badge that this one replaces, if any.
     */
    private String replaces;

    /**
     * A set of users to whom this badge is assigned.
     */
    Set<String> assignments;

    /**
     * A sorted tree map of size multiples with badge images defined to the URL to the image.
     */
    private TreeMap<Integer, String> urls;

    /**
     * Create a new {@link Badge}.
     */
    protected Badge() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Badge badge = (Badge) o;
        return ffzId == badge.ffzId &&
                slot == badge.slot &&
                name.equals(badge.name) &&
                title.equals(badge.title) &&
                Objects.equals(css, badge.css) &&
                Objects.equals(color, badge.color) &&
                Objects.equals(replaces, badge.replaces) &&
                Objects.equals(assignments, badge.assignments) &&
                urls.equals(badge.urls);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, name, title, slot, css, color, replaces, assignments, urls);
    }

    /**
     * Return the URL for the badge image in its base size (1x).
     *
     * @return The URL for the badge image in its base size (1x).
     *
     * @see Badge#getUrl(int)
     */
    @JsonIgnore
    public String getUrl() {
        return getUrl(1);
    }

    /**
     * Return the URL for the badge image for the given size multiplier.
     *
     * <p>
     * This function will provide the best possible image URL for the size requested. Typically FFZ keeps badges in the
     * base size (1x), and can also have a 2x and 4x size. When there is no exact match to9 the size requested this
     * method will return the best match, which is defined as the next larger size if one exists, or the next lower size
     * otherwise.
     * </p>
     *
     * @param size The size multiplier for the URL of the image to get.
     *
     * @return A URL to an badge image which is most appropriate to be rendered at the given size multiplier.
     *
     * @see Badge#getUrl()
     */
    public String getUrl(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException(
                    "Size is a multiplier for the base image size and must be greater than zero.");
        }
        return "https:" + Objects.requireNonNullElse(urls.ceilingEntry(size), urls.floorEntry(size)).getValue();
    }

    /**
     * Returns the full mapping of image URLs.
     *
     * @return The full mapping of image URLs.
     */
    @JsonGetter("urls")
    protected Map<Integer, String> getUrls() {
        return urls;
    }

    /**
     * Returns the ID of the badge in FFZ.
     *
     * @return The ID of the badge in FFZ.
     */
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the name of the badge; this is a simple word that identifies the badge.
     *
     * @return The name of the badge.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the friendly title of the badge; this is the end-user facing description of it.
     *
     * @return The friendly title of the badge.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the slot number of the badge, which determines the order of its display.
     *
     * @return The slot number of the badge.
     */
    public int getSlot() {
        return slot;
    }

    /**
     * Returns the CSS style to apply to the badge's display, if any.
     *
     * @return The CSS style to apply to the badge's display, if any.
     */
    public Optional<String> getCss() {
        return Optional.ofNullable(css);
    }

    /**
     * Returns the background color for the display of the badge, if any.
     *
     * @return The background color for the display of the badge, if any.
     */
    public Optional<String> getColor() {
        return Optional.ofNullable(color);
    }

    /**
     * Returns the name of the Twitch badge this badge replaces, if any.
     *
     * @return The name of the Twitch badge this badge replaces, if any.
     */
    public Optional<String> getReplaces() {
        return Optional.ofNullable(replaces);
    }

    /**
     * Initialize the assignments set.
     *
     * @param size the capacity of the set.
     *
     * @return The new set.
     */
    @Nonnull
    Set<String> initAssignments(final int size) {
        assignments = new HashSet<>(size);
        return assignments;
    }

    /**
     * Returns the set of users to which this badge is assigned, if assignments were returned from the API.
     *
     * @return The set of users to which this badge is assigned, if assignments were returned from the API.
     */
    public Optional<Set<String>> getAssignments() {
        return Optional.ofNullable(assignments);
    }

    /**
     * A JSON deserializer for badges.
     */
    @ParametersAreNonnullByDefault
    static class BadgeDeserializer extends StdDeserializer<Badge> {
        private static final long serialVersionUID = 0;

        /**
         * Constructs a new deserializer.
         */
        BadgeDeserializer() {
            this(null);
        }

        /**
         * Constructs a new deserializer.
         */
        BadgeDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Badge deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var badge = new Badge();

            var badgeNode = rootNode.get("badge");
            if (badgeNode == null) {
                badgeNode = rootNode;
            }

            badge.ffzId = badgeNode.get("id").intValue();
            badge.name = badgeNode.get("name").textValue();
            badge.title = badgeNode.get("title").textValue();
            badge.slot = badgeNode.get("slot").intValue();
            badge.css = badgeNode.path("css").textValue();
            badge.color = badgeNode.path("color").textValue();
            badge.replaces = badgeNode.path("replaces").textValue();

            var subParser = badgeNode.get("urls").traverse(jsonParser.getCodec());
            badge.urls = subParser.readValueAs(URLS_TYPE_REFERENCE);

            var usersNode = rootNode.get("users");
            if (usersNode != null) {
                var iter = usersNode.fields();
                if (iter.hasNext()) {
                    var userList = (ArrayNode) iter.next().getValue();
                    var userSet = badge.initAssignments(userList.size());
                    for (var userNode : userList) {
                        userSet.add(userNode.textValue());
                    }
                }
            }

            return badge;
        }
    }
}
