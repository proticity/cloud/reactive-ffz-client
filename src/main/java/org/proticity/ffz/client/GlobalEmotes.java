/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A class representing the results of a global emote sets query.
 *
 * <p>
 * This class is not exposed publically, but is turned into a stream of {@link EmoteSet} objects.
 * </p>
 *
 * @see EmoteSet
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ParametersAreNonnullByDefault
final class GlobalEmotes implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The map of emote sets.
     */
    private Map<Integer, EmoteSet> emoteSets;

    /**
     * The map of each emote set's ID to the list of users which can use them.
     *
     * <p>
     * An emote set with specific user restrictions is not considered a "default" emote set.
     * </p>
     */
    private Map<String, Set<String>> users;

    /**
     * Constructs a new {@link GlobalEmotes}.
     */
    GlobalEmotes() {
    }

    /**
     * Returns the map of emote sets.
     *
     * @return The map of emote sets.
     */
    @JsonProperty("sets")
    Map<Integer, EmoteSet> getEmoteSets() {
        return emoteSets;
    }

    /**
     * Sets the map of emote sets.
     *
     * @param emoteSets the map of emote sets.
     */
    void setEmoteSets(Map<Integer, EmoteSet> emoteSets) {
        this.emoteSets = emoteSets;
    }

    /**
     * Returns the map of each emote set's ID to the list of users which can use them.
     *
     * @return The map of each emote set's ID to the list of users which can use them.
     */
    @JsonProperty("users")
    Map<String, Set<String>> getUsers() {
        return users;
    }

    /**
     * Sets the map of each emote set's ID to the list of users which can use them.
     *
     * @param users the map of each emote set's ID to the list of users which can use them.
     */
    void setUsers(final Map<String, Set<String>> users) {
        this.users = users;
    }
}
