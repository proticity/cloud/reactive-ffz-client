/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Serializable;
import java.util.List;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A representation of a single page of results from an emote search from the FFZ API.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ParametersAreNonnullByDefault
final class EmoteSearchPage implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The total number of matches in the FFZ database.
     */
    private int totalEmoticons;

    /**
     * The contents of the requested page of results.
     */
    private List<Emote> emoticons;

    /**
     * Construct a new {@link EmoteSearchPage}.
     */
    EmoteSearchPage() {
    }

    /**
     * Returns the total number of matches in the FFZ database.
     *
     * @return The total number of matches in the FFZ database.
     */
    @JsonProperty("_total")
    public int getTotalEmoticons() {
        return totalEmoticons;
    }

    /**
     * Sets the total number of matches in the FFZ database.
     *
     * @param totalEmoticons the total number of matches in the FFZ database.
     */
    public void setTotalEmoticons(int totalEmoticons) {
        this.totalEmoticons = totalEmoticons;
    }

    /**
     * Returns the contents of the requested page of results.
     *
     * @return The contents of the requested page of results.
     */
    @JsonProperty("emoticons")
    public List<Emote> getEmoticons() {
        return emoticons;
    }

    /**
     * Sets the contents of the requested page of results.
     *
     * @param emoticons the contents of the requested page of results.
     */
    public void setEmoticons(List<Emote> emoticons) {
        this.emoticons = emoticons;
    }
}
