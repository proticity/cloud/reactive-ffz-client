/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A collection of all badges across FFZ.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Badges implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The type reference for an individual badge.
     */
    private static final TypeReference<Badge> BADGE_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * The mapping of badge IDs to the badges.
     */
    private Map<Integer, Badge> badges;

    /**
     * The mapping of usernames to a map of badges they are assigned.
     */
    private Map<String, Map<Integer, Badge>> badgeAssignments;

    /**
     * Indicates if this badge data includes the user assignments.
     */
    private boolean hasAssignments;

    /**
     * Constructs a new {@link Badges} object.
     */
    protected Badges() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Badges badges1 = (Badges) o;
        return hasAssignments == badges1.hasAssignments &&
                badges.equals(badges1.badges);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(badges);
    }

    /**
     * Returns the set of badges that exist in FFZ, mapped by their FFZ ID.
     *
     * @return The set of badges that exist in FFZ, mapped by their FFZ ID.
     */
    @JsonGetter("badges")
    public Map<Integer, Badge> getBadges() {
        return badges;
    }

    /**
     * Returns a mapping of usernames to the set of badges they are assigned, if the API returned this information.
     *
     * @return A mapping of usernames to the set of badges they are assigned, if the API returned this information.
     */
    @JsonIgnore
    public Optional<Map<String, Map<Integer, Badge>>> getBadgeAssignments() {
        if (badgeAssignments == null && hasAssignments) {
            createUserBadgeMapping();
        }
        return Optional.ofNullable(badgeAssignments);
    }

    /**
     * Returns whether this badge set includes user assignments.
     *
     * <p>This property is used for serializing/deserializing this type more efficiently.</p>
     *
     * @return Whether this badge set includes user assignments.
     */
    @JsonGetter("hasAssignments")
    protected boolean hasAssignments() {
        return hasAssignments;
    }

    /**
     * Lazily builds the original user badge assignments from the normalized data after deserialization.
     */
    private void createUserBadgeMapping() {
        badgeAssignments = new HashMap<>();
        for (var badge : badges.values()) {
            var assignments = badge.getAssignments();
            if (assignments.isPresent()) {
                for (var user : assignments.get()) {
                    var userBadgeAssignments = badgeAssignments.computeIfAbsent(user, key -> new HashMap<>());
                    userBadgeAssignments.put(badge.getFfzId(), badge);
                }
            }
        }
    }

    /**
     * A JSON deserializer for the set of badges.
     */
    @ParametersAreNonnullByDefault
    static class BadgesDeserializer extends StdDeserializer<Badges> {
        private static final long serialVersionUID = 0;

        /**
         * A logger for recording warnings.
         */
        private static final Logger LOGGER = LoggerFactory.getLogger(BadgesDeserializer.class);

        /**
         * Constructs a default {@link Badge.BadgeDeserializer}.
         */
        BadgesDeserializer() {
            this(null);
        }

        /**
         * Constructs a new {@link Badge.BadgeDeserializer}.
         */
        BadgesDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Badges deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var badges = new Badges();

            var badgesNode = (ArrayNode) rootNode.get("badges");
            badges.badges = new HashMap<>(badgesNode.size());
            for (var badgeNode : badgesNode) {
                var subParser = badgeNode.traverse(jsonParser.getCodec());
                Badge badge = subParser.readValueAs(BADGE_TYPE_REFERENCE);
                badges.badges.put(badge.getFfzId(), badge);
            }

            var usersNode = rootNode.get("users");
            if (usersNode == null) {
                return badges;
            }
            badges.hasAssignments = true;
            var usersMap = new HashMap<String, Map<Integer, Badge>>();
            badges.badgeAssignments = usersMap;
            var iter = usersNode.fields();
            while (iter.hasNext()) {
                var field = iter.next();
                var badgeId = Integer.parseInt(field.getKey());
                var badge = badges.badges.get(badgeId);
                if (badge == null) {
                    LOGGER.warn("User has reference to non-existent badge by ID {}.", badgeId);
                    continue;
                }
                var userList = (ArrayNode) field.getValue();
                var userSet = badge.initAssignments(userList.size());
                for (var userNode : userList) {
                    userSet.add(userNode.textValue());
                }
            }

            for (var badge : badges.badges.values()) {
                badge.getAssignments().ifPresentOrElse(users -> {
                    for (var user : users) {
                        var userBadgeSet = usersMap.computeIfAbsent(user, key -> new HashMap<>());
                        userBadgeSet.put(badge.getFfzId(), badge);
                    }
                }, () -> badge.assignments = Collections.emptySet());
            }

            return badges;
        }
    }
}
