/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Contains a reference to a user with abbreviated information.
 *
 * @see Emote
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class UserReference implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The ID of the user in FFZ.
     */
    int ffzId;

    /**
     * The Twitch username for the user.
     */
    String username;

    /**
     * Returns the user's Twitch display name.
     */
    String displayName;

    /**
     * Constructs a new {@link UserReference}.
     */
    protected UserReference() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserReference that = (UserReference) o;
        return ffzId == that.ffzId &&
                Objects.equals(username, that.username) &&
                Objects.equals(displayName, that.displayName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, username);
    }

    /**
     * Returns the ID of the user in FFZ.
     *
     * @return The ID of the user in FFZ.
     */
    @JsonGetter("ffzId")
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the Twitch username for the user (all lowercase as per Twitch's behavior).
     *
     * @return The Twitch username for the user.
     */
    @JsonGetter("username")
    public String getUsername() {
        return username;
    }

    /**
     * Returns the user's Twitch display name (their username with custom capitalization).
     *
     * @return The user's Twitch display name
     */
    @JsonGetter("displayName")
    public String getDisplayName() {
        return displayName;
    }
}
