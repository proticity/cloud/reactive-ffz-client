/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Describes a custom moderator badge for a channel.
 *
 * @see Badge
 * @see Channel
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class ModeratorBadge implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The map of size multipliers to the image of that size.
     */
    private TreeMap<Integer, String> urls;

    /**
     * Constructs a new {@link ModeratorBadge}.
     */
    protected ModeratorBadge() {
    }

    /**
     * Instantiates a new {@link ModeratorBadge}.
     *
     * @param urls the mapping of size multipliers to badge image URLs.
     */
    ModeratorBadge(TreeMap<Integer, String> urls) {
        this.urls = urls;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModeratorBadge that = (ModeratorBadge) o;
        return urls.equals(that.urls);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(urls);
    }

    /**
     * Returns the URL to the image of the badge in the base size.
     *
     * @return The URL to the image of the badge in the base size.
     */
    @JsonIgnore
    String getUrl() {
        return getUrl(1);
    }

    /**
     * Returns the URL to the image of the badge in the best possible size for the requested size multiplier.
     *
     * <p>
     * When no exact size match is found, returns the next higher size multplier (to be scaled down) if one exists.
     * Otherwise, finds the next smallest size, to be scaled up.
     * </p>
     *
     * @param size the multiplier for the requested size relative to Twitch's base emote size.
     *
     * @return A URL to the best matching moderator badge image.
     */
    public String getUrl(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException(
                    "The size argument indicates a size multiplier and must be greater than zero.");
        }
        var url = urls.ceilingEntry(size);
        if (url == null) {
            url = urls.floorEntry(size);
        }
        return "https:" + url.getValue();
    }

    /**
     * Returns the full set of URLs for the badge images.
     *
     * @return The full set of URLs for the badge images.
     */
    @JsonGetter("urls")
    protected Map<Integer, String> getUrls() {
        return urls;
    }
}
