/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * An exception indicating an error interacting with the FFZ API.
 */
@ParametersAreNonnullByDefault
public class FfzApiException extends RuntimeException {
    /**
     * Constructs a new {@link FfzApiException} with a custom message.
     *
     * @param message the error message.
     */
    public FfzApiException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@link FfzApiException} with a custom message and based on another exception.
     *
     * @param message the error message.
     * @param cause the exception that was the cause of the FFZ API error.
     */
    public FfzApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
