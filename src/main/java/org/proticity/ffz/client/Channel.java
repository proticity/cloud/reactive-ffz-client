/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Description of channel information from the FFZ API.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Channel {
    // TODO: Make serializable.

    /**
     * A type reference for emote set maps.
     */
    private static final TypeReference<Map<Integer, Optional<EmoteSet>>> EMOTE_SETS_TYPE_REFERENCE =
            new TypeReference<>() { };

    /**
     * The unique ID for this channel in FFZ.
     */
    private int ffzId;

    /**
     * The unique ID for this channel in Twitch.
     */
    private int twitchId;

    /**
     * The display name of the Twitch channel (same as the display name of the user who owns it).
     */
    private String displayName;

    /**
     * The name of the channel (same as the username of the user who owns it).
     */
    private String name;

    /**
     * CSS styles to apply to this channel when displayed, if any.
     */
    private String css;

    /**
     * Whether this channel is a group.
     */
    private boolean group;

    /**
     * The custom moderator badge information for this channel, if it has a custom badge.
     */
    private ModeratorBadge moderatorBadge;

    /**
     * A set of the emote set IDs for this channel, including a mapping to the set information if it was included in the
     * API response.
     */
    private Map<Integer, Optional<EmoteSet>> emoteSets;

    /**
     * Constructs a new {@link Channel}.
     */
    protected Channel() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Channel channel = (Channel) o;
        return ffzId == channel.ffzId &&
                twitchId == channel.twitchId &&
                group == channel.group &&
                displayName.equals(channel.displayName) &&
                name.equals(channel.name) &&
                Objects.equals(css, channel.css) &&
                Objects.equals(moderatorBadge, channel.moderatorBadge) &&
                emoteSets.equals(channel.emoteSets);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, twitchId, displayName, name, css, group, moderatorBadge, emoteSets);
    }

    /**
     * Returns the unique ID for this channel in FFZ.
     *
     * @return The unique ID for this channel in FFZ.
     */
    @JsonGetter("ffzId")
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the unique ID for this channel in Twitch.
     *
     * @return The unique ID for this channel in Twitch.
     */
    @JsonGetter("twitchId")
    public int getTwitchId() {
        return twitchId;
    }

    /**
     * Returns the display name of the Twitch channel (same as the display name of the user who owns it).
     *
     * @return The display name of the Twitch channel (same as the display name of the user who owns it).
     */
    @JsonGetter("displayName")
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns the name of the channel (same as the username of the user who owns it).
     *
     * @return The name of the channel (same as the username of the user who owns it).
     */
    @JsonGetter("name")
    public String getName() {
        return name;
    }

    /**
     * Returns CSS styles to apply to this channel when displayed, if any.
     *
     * @return CSS styles to apply to this channel when displayed, if any.
     */
    @JsonGetter("css")
    public Optional<String> getCss() {
        return Optional.ofNullable(css);
    }

    /**
     * Returns whether this channel is a group.
     *
     * @return Whether this channel is a group.
     */
    @JsonGetter("isGroup")
    public boolean isGroup() {
        return group;
    }

    /**
     * Returns the custom moderator badge information for this channel, if it has a custom badge.
     *
     * @return The custom moderator badge information for this channel, if it has a custom badge.
     */
    @JsonGetter("moderatorBadge")
    public Optional<ModeratorBadge> getModeratorBadge() {
        return Optional.ofNullable(moderatorBadge);
    }

    /**
     * Returns a set of the emote set IDs for this channel, including a mapping to the set information if it was
     * included in the API response.
     *
     * @return A mapping of emote set IDs for this channel to emote set information, or mapped to nothing if no emote
     *         set information was included in the response.
     */
    @JsonGetter("emoteSets")
    public Map<Integer, Optional<EmoteSet>> getEmoteSets() {
        return emoteSets;
    }

    /**
     * A JSON deserializer for a {@link Channel}.
     */
    @ParametersAreNonnullByDefault
    static final class ChannelDeserializer extends StdDeserializer<Channel> {
        private static final long serialVersionUID = 0;

        /**
         * Constructs a default {@link ChannelDeserializer}.
         */
        ChannelDeserializer() {
            this(null);
        }

        /**
         * Constructs a new {@link ChannelDeserializer}.
         */
        ChannelDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Channel deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var channel = new Channel();

            var roomNode = rootNode.get("room");
            channel.ffzId = roomNode.get("_id").asInt();
            channel.twitchId = roomNode.get("twitch_id").asInt();
            channel.name = roomNode.get("id").textValue();
            channel.displayName = roomNode.get("display_name").textValue();
            channel.css = roomNode.path("css").textValue();
            channel.group = roomNode.path("is_group").asBoolean();
            var modUrls = (ObjectNode) roomNode.get("mod_urls");
            if (modUrls != null) {
                var urls = new TreeMap<Integer, String>();
                var iter = modUrls.fields();
                while (iter.hasNext()) {
                    var field = iter.next();
                    if (field.getValue().textValue() != null) {
                        urls.put(Integer.parseInt(field.getKey()), field.getValue().textValue());
                    }
                }
                channel.moderatorBadge = new ModeratorBadge(urls);
            }

            var sets = rootNode.get("sets");
            if (sets != null) {
                var nestedParser = sets.traverse(jsonParser.getCodec());
                channel.emoteSets = nestedParser.readValueAs(EMOTE_SETS_TYPE_REFERENCE);
            } else {
                var setId = roomNode.get("set").asInt();
                channel.emoteSets = Collections.singletonMap(setId, Optional.empty());
            }

            return channel;
        }
    }
}
