/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Contains data about an FFZ/Twitch user.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class User {
    // TODO: Make serializable.

    /**
     * Type reference for the mapping of badges.
     */
    private static final TypeReference<Map<Integer, Optional<Badge>>> BADGES_TYPE_REFERENCES =
            new TypeReference<>() { };

    /**
     * Type reference for the mapping of emote sets.
     */
    private static final TypeReference<Map<Integer, Optional<EmoteSet>>> EMOTE_SETS_TYPE_REFERENCE =
            new TypeReference<>() { };

    /**
     * The user's unique ID in FFZ.
     */
    private int ffzId;

    /**
     * The user's username on Twitch, in all lowercase as per the reference username on Twitch.
     */
    private String username;

    /**
     * The user's display name on Twitch.
     */
    private String displayName;

    /**
     * Whether this user is a FFZ donor.
     */
    private boolean donor;

    /**
     * The unique Twitch ID of the user.
     */
    private int twitchId;

    /**
     * A mapping of the user's badges, mapped by the badge's unique ID.
     */
    private Map<Integer, Optional<Badge>> badges;

    /**
     * The URL of the user's avatar image on Twitch.
     */
    private String avatar;

    /**
     * A mapping of the user's emote sets, mapped by the emote set's unique ID.
     */
    private Map<Integer, Optional<EmoteSet>> emoteSets;

    /**
     * Constructs a new {@link User}.
     */
    protected User() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return ffzId == user.ffzId &&
                donor == user.donor &&
                twitchId == user.twitchId &&
                username.equals(user.username) &&
                displayName.equals(user.displayName) &&
                badges.equals(user.badges) &&
                avatar.equals(user.avatar) &&
                emoteSets.equals(user.emoteSets);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, username, displayName, donor, twitchId, badges, avatar, emoteSets);
    }

    /**
     * Returns the user's unique ID in FFZ.
     *
     * @return The user's unique ID in FFZ.
     */
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the user's username on Twitch, in all lowercase as per the reference username on Twitch.
     *
     * @return The user's username on Twitch.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the user's display name on Twitch, i.e. their username with customized capitalization.
     *
     * @return The user's display name on Twitch.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns whether this user is a FFZ donor.
     *
     * @return Whether this user is a FFZ donor.
     */
    @JsonGetter("isDonor")
    public boolean isDonor() {
        return donor;
    }

    /**
     * Returns the users unique Twitch ID.
     *
     * @return The users unique Twitch ID.
     */
    public int getTwitchId() {
        return twitchId;
    }

    /**
     * Returns a mapping of the user's badges, mapped by the badge's unique ID.
     *
     * <p>
     * If the user's badge information was not requested, this will have the badge IDs as keys but the {@link Badge}
     * will not be included.
     * </p>
     *
     * @return A mapping of the user's badges, mapped by the badge's unique ID.
     */
    public Map<Integer, Optional<Badge>> getBadges() {
        return badges;
    }

    /**
     * Returns the URL to the avatar image for the user.
     *
     * @return The URL to the avatar image for the user.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Returns a mapping of the user's emote sets, mapped by the emote set's unique ID.
     *
     * <p>
     * The user's information was requested without emote sets this mapping will include the emote set IDs as keys but
     * will not have an {@link EmoteSet} value.
     * </p>
     *
     * @return A mapping of the user's emote sets, mapped by the emote set's unique ID.
     */
    public Map<Integer, Optional<EmoteSet>> getEmoteSets() {
        return emoteSets;
    }

    /**
     * A JSON deserializer for {@link User} objects.
     */
    static class UserDeserializer extends StdDeserializer<User> {
        private static final long serialVersionUID = 0;

        UserDeserializer() {
            this(null);
        }

        UserDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        @Override
        public User deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var user = new User();

            var userNode = rootNode.get("user");
            user.ffzId = userNode.get("id").asInt();
            user.displayName = userNode.get("display_name").textValue();
            user.username = userNode.get("name").textValue();
            user.twitchId = userNode.get("twitch_id").asInt();
            user.avatar = userNode.get("avatar").textValue();
            user.donor = userNode.path("is_donor").asBoolean();

            var badgesNode = rootNode.get("badges");
            if (badgesNode != null) {
                var subParser = badgesNode.traverse(jsonParser.getCodec());
                user.badges = subParser.readValueAs(BADGES_TYPE_REFERENCES);
            } else {
                badgesNode = userNode.get("badges");
                user.badges = new HashMap<>(badgesNode.size());
                for (var id : badgesNode) {
                    user.badges.put(id.asInt(), Optional.empty());
                }
            }

            var emoteSetsNode = rootNode.get("sets");
            if (emoteSetsNode != null) {
                var subParser = emoteSetsNode.traverse(jsonParser.getCodec());
                user.emoteSets = subParser.readValueAs(EMOTE_SETS_TYPE_REFERENCE);
            } else {
                emoteSetsNode = userNode.get("emote_sets");
                user.emoteSets = new HashMap<>(emoteSetsNode.size());
                for (var id : emoteSetsNode) {
                    user.emoteSets.put(id.asInt(), Optional.empty());
                }
            }

            return user;
        }
    }
}
