/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

/**
 * Indicates the type of an {@link EmoteSet}.
 *
 * @see EmoteSet
 */
public enum EmoteSetType {
    /**
     * An {@link EmoteSet} which applies globally (although possibly restricted to specific users).
     */
    GLOBAL,

    /**
     * An {@link EmoteSet} which belongs to a channel.
     */
    CHANNEL,

    /**
     * An {@link EmoteSet} of unknown or unspecified type.
     */
    UNKNOWN,
}
