/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

/**
 * Indicates the sort order for emote results in an emote search.
 */
public enum SortOrder {
    /**
     * Sort by the emote creation date, in ascending order.
     */
    CREATED_ASC,

    /**
     * Sort by the emote creation date, in descending order.
     */
    CREATED_DESC,

    /**
     * Sort by the emote's name, in ascending order.
     */
    NAME_ASC,

    /**
     * Sort by the emote's name, in descending order.
     */
    NAME_DESC,

    /**
     * Sort by the emote's last updated date, in ascending order.
     */
    UPDATED_ASC,

    /**
     * Sort by the emote's last updated date, in descending order.
     */
    UPDATED_DESC,

    /**
     * Sort by the number of channels using the emote, in ascending order.
     */
    USE_COUNT_ASC,

    /**
     * Sort by the number of channels using the emote, in descending order.
     */
    USE_COUNT_DESC,

    /**
     * Sort by the name of the channel which owns the emote, in ascending order.
     */
    OWNER_ASC,

    /**
     * Sort by the name of the channel which owns the emote, in descending order.
     */
    OWNER_DESC
}
