/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import reactor.util.annotation.Nullable;

/**
 * Represents a container for a group of emotes which collectively applies ownership and permissions to its emotes.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmoteSet implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A reference to the type of an individual emote.
     */
    private static final TypeReference<Emote> EMOTE_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * The unique ID of the emote within FFZ.
     */
    private int ffzId;

    /**
     * A CSS style to apply to the emote set when displayed, if any.
     */
    private String css;

    /**
     * A user-friendly description of the emote set, if it has one.
     */
    private String description;

    /**
     * A user-friendly title for the emote set.
     */
    private String title;

    /**
     * The URL of an icon to display for the emote set, if any.
     */
    private String icon;

    /**
     * The type of the emote set.
     */
    private EmoteSetType type = EmoteSetType.UNKNOWN;

    /**
     * A mapping of the emotes in the emote set, mapped by their name.
     */
    private Map<String, Emote> emotes;

    /**
     * The set of users permitted to use the emote set, if this information was requested from the API.
     */
    private Set<String> permittedUsers;

    /**
     * Instantiate a new {@link EmoteSet}.
     */
    protected EmoteSet() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmoteSet emoteSet = (EmoteSet) o;
        return ffzId == emoteSet.ffzId &&
                Objects.equals(css, emoteSet.css) &&
                Objects.equals(description, emoteSet.description) &&
                title.equals(emoteSet.title) &&
                Objects.equals(icon, emoteSet.icon) &&
                type == emoteSet.type &&
                emotes.equals(emoteSet.emotes) &&
                Objects.equals(permittedUsers, emoteSet.permittedUsers);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, css, description, title, icon, type, emotes, permittedUsers);
    }

    /**
     * Returns the unique ID of the emote within FFZ.
     *
     * @return The unique ID of the emote within FFZ.
     */
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the CSS style to apply to the emote set when displayed, if any.
     *
     * @return CSS style to apply to the emote set when displayed, if any.
     */
    @Nonnull
    public Optional<String> getCss() {
        return Optional.ofNullable(css);
    }

    /**
     * Returns a user-friendly description of the emote set, if it has one.
     *
     * @return A user-friendly description of the emote set, if it has one.
     */
    @Nonnull
    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    /**
     * Returns a user-friendly title for the emote set.
     *
     * @return A user-friendly title for the emote set.
     */
    @Nonnull
    public String getTitle() {
        return title;
    }

    /**
     * The URL of an icon to display for the emote set, if any.
     *
     * @return The URL of an icon to display for the emote set, if any.
     */
    @Nonnull
    public Optional<String> getIcon() {
        return Optional.ofNullable(icon);
    }

    /**
     * Returns the type of the emote set.
     *
     * @return The type of the emote set.
     */
    @Nonnull
    public EmoteSetType getType() {
        return type;
    }

    /**
     * Returns a mapping of the emotes in the emote set, mapped by their name (the text they replace).
     *
     * @return A mapping of the emotes in the emote set, mapped by their name.
     */
    @Nonnull
    public Map<String, Emote> getEmotes() {
        return emotes;
    }

    /**
     * Initializes the permitted users set.
     *
     * @param size the capacity of the set.
     *
     * @return The initialized set.
     */
    @Nonnull
    Set<String> initPermittedUsers(final int size) {
        permittedUsers = new HashSet<>(size);
        return permittedUsers;
    }

    /**
     * The set of users permitted to use the emote set, if this information was requested from the API.
     *
     * @return The set of users permitted to use the emote set, if this information was requested from the API.
     */
    @Nonnull
    public Optional<Set<String>> getPermittedUsers() {
        return Optional.ofNullable(permittedUsers);
    }

    /**
     * A JSON deserializer for emote sets.
     */
    static class EmoteSetDeserializer extends StdDeserializer<EmoteSet> {
        private static final long serialVersionUID = 0;

        /**
         * Constructs a new {@link EmoteSetDeserializer}.
         */
        EmoteSetDeserializer() {
            this(null);
        }

        /**
         * Constructs a new {@link EmoteSetDeserializer}.
         */
        EmoteSetDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public EmoteSet deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var emoteSet = new EmoteSet();

            var emoteSetNode = rootNode.get("set");
            if (emoteSetNode == null) {
                emoteSetNode = rootNode;
            }

            emoteSet.ffzId = emoteSetNode.get("id").asInt();
            emoteSet.title = emoteSetNode.get("title").textValue();
            emoteSet.css = emoteSetNode.path("css").textValue();
            emoteSet.description = emoteSetNode.path("description").textValue();
            emoteSet.icon = emoteSetNode.path("icon").textValue();
            switch (emoteSetNode.get("_type").asInt()) {
                case 0:
                    emoteSet.type = EmoteSetType.GLOBAL;
                    break;
                case 1:
                    emoteSet.type = EmoteSetType.CHANNEL;
                    break;
                default:
                    emoteSet.type = EmoteSetType.UNKNOWN;
            }

            var emoteNodes = (ArrayNode) emoteSetNode.get("emoticons");
            if (emoteNodes != null) {
                emoteSet.emotes = new HashMap<>(emoteNodes.size());
                for (var emoteNode : emoteNodes) {
                    var subParser = emoteNode.traverse(jsonParser.getCodec());
                    Emote emote = subParser.readValueAs(EMOTE_TYPE_REFERENCE);
                    emoteSet.emotes.put(emote.getName(), emote);
                }
            }

            var usersNode = rootNode.get("users");
            if (usersNode != null) {
                var iter = usersNode.fields();
                if (iter.hasNext()) {
                    var userList = (ArrayNode) iter.next().getValue();
                    emoteSet.permittedUsers = new HashSet<>(userList.size());
                    for (var userNode : userList) {
                        emoteSet.permittedUsers.add(userNode.textValue());
                    }
                }
            }

            return emoteSet;
        }
    }
}
