/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.Closeable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Locale;
import java.util.function.Consumer;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.netty.handler.codec.http.HttpResponseStatus;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.netty.http.HttpResources;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.tcp.SslProvider;

/**
 * A client for retrieving information from the FFZ API.
 */
@ParametersAreNonnullByDefault
public class FfzClient implements Closeable {
    /**
     * The default timeout on dispose, in seconds.
     */
    private static final int DEFAULT_DISPOSE_TIMEOUT = 3;

    /**
     * A copy of the builder that was used to construct the client.
     */
    private FfzClientBuilder builder;

    /**
     * The client that will be used to issue HTTP requests.
     */
    private HttpClient httpClient;

    /**
     * Constructs a new {@link FfzClient}.
     *
     * @param builder a builder with the options used to construct the client.
     */
    protected FfzClient(FfzClientBuilder builder) {
        this.builder = new FfzClientBuilder(builder);

        // Setup the Jackson deserializers.
        var jacksonModule = new SimpleModule();
        jacksonModule.addDeserializer(Badge.class, new Badge.BadgeDeserializer());
        jacksonModule.addDeserializer(Badges.class, new Badges.BadgesDeserializer());
        jacksonModule.addDeserializer(Channel.class, new Channel.ChannelDeserializer());
        jacksonModule.addDeserializer(Emote.class, new Emote.EmoteDeserializer());
        jacksonModule.addDeserializer(EmoteSet.class, new EmoteSet.EmoteSetDeserializer());
        jacksonModule.addDeserializer(User.class, new User.UserDeserializer());
        this.builder.objectMapper.registerModules(jacksonModule, new Jdk8Module());

        // Create the HTTP client.
        httpClient = HttpClient.create(builder.connectionProvider)
                .baseUrl(builder.restEndpoint);
        if (builder.secure) {
            if (builder.sslProviderBuilder == null) {
                httpClient = httpClient.secure();
            } else {
                httpClient = httpClient.secure(builder.sslProviderBuilder);
            }
        }
    }

    /**
     * Returns information about a given badge and the users to whom it is assigned.
     *
     * @param badgeId the numeric FFZ ID for the badge.
     *
     * @return The badge information, including the list of users who have it.
     */
    public Mono<Badge> badge(int badgeId) {
        return badge(badgeId, true);
    }

    /**
     * Returns information about a given badge and the users to whom it is assigned.
     *
     * @param badgeId the numeric FFZ ID or name for the badge.
     *
     * @return The badge information, including the list of users who have it.
     */
    public Mono<Badge> badge(Mono<?> badgeId) {
        return badge(badgeId, true);
    }

    /**
     * Returns information about a given badge and the users to whom it is assigned.
     *
     * @param badgeName the name of the badge.
     *
     * @return The badge information, including the list of users who have it.
     */
    public Mono<Badge> badge(String badgeName) {
        return badge(badgeName, true);
    }

    /**
     * Returns information about a given badge.
     *
     * @param badgeId the numeric FFZ ID for the badge.
     * @param includeAssignments whether to include the list of users who have the badge.
     *
     * @return The badge information.
     */
    public Mono<Badge> badge(int badgeId, boolean includeAssignments) {
        return badge(Mono.just(badgeId), includeAssignments);
    }

    /**
     * Returns information about a given badge.
     *
     * @param badgeId A publisher of the numeric FFZ ID for the badge or the string name of the badge.
     * @param includeAssignments whether to include the list of users who have the badge.
     *
     * @return The badge information.
     */
    public Mono<Badge> badge(Mono<?> badgeId, boolean includeAssignments) {
        var badgePublisher = badgeId.doOnNext(id -> {
            if (!(id instanceof String) && !(id instanceof Integer)) {
                throw new IllegalArgumentException("Publisher must yield a badge ID or name.");
            }
        }).map(id -> (includeAssignments ? "/badge/" : "/_badge/") + id.toString());
        return request(badgePublisher, Badge.class);
    }

    /**
     * Returns information about a given badge.
     *
     * @param badgeName the name of the badge.
     * @param includeAssignments whether to include the list of users who have the badge.
     *
     * @return The badge information.
     */
    public Mono<Badge> badge(String badgeName, boolean includeAssignments) {
        return request(Mono.just((includeAssignments ? "/badge/" : "/_badge/") + badgeName), Badge.class);
    }

    /**
     * Returns the entire set of badges available on FFZ, including user assignments.
     *
     * @return The entire set of badges available on FFZ, including user assignments.
     */
    public Mono<Badges> badges() {
        return badges(true);
    }

    /**
     * Returns the entire set of badges available on FFZ with or without the list of users they are assigned to.
     *
     * @param includeAssignments whether to include the list of users badges are assigned to.
     *
     * @return The list of FFZ badges.
     */
    public Mono<Badges> badges(boolean includeAssignments) {
        return request(Mono.just(includeAssignments ? "/badges" : "/_badges"),
                Badges.class);
    }

    /**
     * Returns the information for a channel in FFZ, including all channel emotes.
     *
     * @param channelId the Twitch ID of the channel.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(int channelId) {
        return channel(channelId, true);
    }

    /**
     * Returns the information for a channel in FFZ, including all channel emotes.
     *
     * @param channelName the name of the channel.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(String channelName) {
        return channel(channelName, true);
    }

    /**
     * Returns the information for a channel in FFZ, including all channel emotes.
     *
     * @param channelId a publisher for the channel's name or ID on Twitch.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(Mono<?> channelId) {
        return channel(channelId, true);
    }

    /**
     * Returns the information for a channel in FFZ, optionally including channel emotes.
     *
     * @param channelName the name of the channel.
     * @param includeEmotes whether channel emote sets should be included.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(String channelName, boolean includeEmotes) {
        return request(Mono.just((includeEmotes ? "/room/" : "/_room/") + channelName.toLowerCase(Locale.ROOT)),
                Channel.class);
    }

    /**
     * Returns the information for a channel in FFZ, including all channel emotes.
     *
     * @param channelId the Twitch ID of the channel.
     * @param includeEmotes whether channel emote sets should be included.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(int channelId, boolean includeEmotes) {
        return request(Mono.just((includeEmotes ? "/room/id/" : "/_room/id/") + channelId), Channel.class);
    }

    /**
     * Returns the information for a channel in FFZ, optionally include channel emotes.
     *
     * @param channelId a publisher for the channel's name or ID on Twitch.
     * @param includeEmotes whether channel emote sets should be included.
     *
     * @return The channel information.
     */
    public Mono<Channel> channel(Mono<?> channelId, boolean includeEmotes) {
        var channelPublisher = channelId.map(id -> {
            if (id instanceof String) {
                return (includeEmotes ? "/room/" : "/_room/") + id.toString().toLowerCase(Locale.ROOT);
            } else if (id instanceof Integer) {
                return (includeEmotes ? "/room/id/" : "/_room/id/") + id.toString();
            } else {
                throw new IllegalArgumentException("Publisher must yield a channel ID or name.");
            }
        });
        return request(channelPublisher, Channel.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        dispose();
    }

    /**
     * Dispose of all resources for the client asynchronously.
     */
    public void dispose() {
        // No resources currently require disposal.
    }

    /**
     * Synchronously dispose of the client's resources, waiting at most 3 seconds.
     */
    public void disposeNow() {
        disposeNow(Duration.ofSeconds(DEFAULT_DISPOSE_TIMEOUT));
    }

    /**
     * Synchronously dispose of the client's resources, with a given timeout.
     *
     * @param timeout the maximum time to wait for the disposal.
     */
    public void disposeNow(Duration timeout) {
        // No resources currently require disposal.
    }

    /**
     * Gets the information for a specific emote.
     *
     * @param emoteId the numeric FFZ ID for the emote.
     *
     * @return The information for a specific emote.
     */
    public Mono<Emote> emote(int emoteId) {
        return request(Mono.just("/emote/" + emoteId), Emote.class);
    }

    /**
     * Gets the information for a specific emote.
     *
     * @param emoteId a publisher of the emote ID.
     *
     * @return The information for a specific emote.
     */
    public Mono<Emote> emote(Mono<Integer> emoteId) {
        return request(emoteId.map(id -> "/emote/" + id), Emote.class);
    }

    /**
     * The internal state class which keeps state between each generation call for the emote search results Flux.
     */
    private static final class SearchState {
        int pageSize;
        int maxResults;
        String sortOrder;
        int page = 1;

        SearchState() {
        }
    }

    /**
     * Retrieves a stream of all matchines results in FrankerFaceZ.
     *
     * <p>
     * This API acts as a search through potentially all possible FFZ results. The parameters of the results are
     * determined by the <code>param</code> argument. A single page of results can be retrieved if a <code>page</code>
     * parameter is set on that object; otherwise the client will automatically proceed page by page streaming results.
     * The maximum number of results returned can be controlled by the <code>maxResults</code> property of
     * theparameters, but if it is <code>null</code> all possible matched results are retrieved.
     * </p>
     *
     * <p>
     * Other possible filters are to query by substring match of the emote name, and to include non-public results. It
     * is possible to restrict results to only "high DPI" results, i.e. those which have more than the default small
     * size. A sort order can also be specified, with the default being by creation date, descending.
     * </p>
     *
     * <p>
     * Results are provided as a {@link Flux} of results and {@link Flux} of total matching emote counts. The matching
     * count is the total number of matching results, and the number is signaled to the publisher once per page of
     * results fetched. Since streaming numerous pages can take time very general queries may see this number change
     * between pages as new results are added and approved. The consumer can choose whether to simply take the first
     * result, or wait for the last result, or count the actual results streamed out of the publisher.
     * </p>
     *
     * @param params the parameters for the emote listing.
     *
     * @return An {@link ConnectableEmoteResults} which has handles to the streams of data returned.
     */
    public ConnectableEmoteResults emotes(EmoteSearch params) {
        final var state = new SearchState();
        switch (params.sortOrder) {
            case NAME_ASC:
                state.sortOrder = "name-asc";
                break;
            case NAME_DESC:
                state.sortOrder = "name-desc";
                break;
            case UPDATED_ASC:
                state.sortOrder = "updated-asc";
                break;
            case UPDATED_DESC:
                state.sortOrder = "updated-desc";
                break;
            case USE_COUNT_ASC:
                state.sortOrder = "count-asc";
                break;
            case USE_COUNT_DESC:
                state.sortOrder = "count-desc";
                break;
            case OWNER_ASC:
                state.sortOrder = "owner-asc";
                break;
            case OWNER_DESC:
                state.sortOrder = "order-desc";
                break;
            case CREATED_ASC:
                state.sortOrder = "created-asc";
                break;
            default:
                state.sortOrder = "created-desc";
                break;
        }

        state.pageSize = params.pageSize;
        state.maxResults = params.maxResults;
        if (params.page != null) {
            state.page = params.page;
            state.maxResults = state.pageSize = Math.min(state.pageSize, state.maxResults);
        }

        var results = new ConnectableEmoteResults();
        results.source = Flux.create((FluxSink<EmoteSearchPage> sink) -> {
            while (state.maxResults > 0) {
                try {
                    var pageResult = emotes(params, state.sortOrder, state.page, state.maxResults, state.pageSize)
                            .blockOptional(params.pageTimeout);
                    if (pageResult.isPresent()) {
                        var page = pageResult.get();
                        sink.next(page);
                        state.maxResults -= page.getEmoticons().size();
                        state.page++;
                    } else {
                        sink.error(new FfzApiException("FFZ emote search API returned no results for a page."));
                        break;
                    }
                } catch (IllegalStateException e) {
                    sink.error(new FfzApiException(("Timeout during retrieval of emote page " + state.page)));
                    break;
                }
            }
            sink.complete();
        }).publish();
        return results;
    }

    /**
     * Issues a request for a single page of emote results.
     *
     * @param params the parameters given for the search.
     * @param sortOrder the sort order for the results, pre-converted to a string.
     * @param currentPage the page to retrieve with this request.
     * @param remainingResults the number of max results to get.
     * @param pageSize the max size of the page to return.
     *
     * @return The results in the requested page.
     */
    private Mono<EmoteSearchPage> emotes(EmoteSearch params, String sortOrder, int currentPage,
                                         int remainingResults, int pageSize) {

        var queryBuilder = new StringBuilder("/emoticons?");
        queryBuilder.append("per_page=").append(Math.min(pageSize, remainingResults));
        queryBuilder.append("&page=").append(currentPage);
        queryBuilder.append("&high_dpi=").append(params.highDpiRequired ? "on" : "off");
        queryBuilder.append("&private=").append(params.publicOnly ? "off" : "on");
        queryBuilder.append("&sort=").append(sortOrder);
        if (params.nameFilter != null) {
            queryBuilder.append("&q=").append(URLEncoder.encode(params.nameFilter, StandardCharsets.UTF_8));
        }

        return request(Mono.just(queryBuilder.toString()), EmoteSearchPage.class);
    }

    /**
     * Gets the information for a specific emote set, including all results it contains.
     *
     * @param emoteSetId the numeric FFZ ID for the emote set.
     *
     * @return The information for the emote set and all results it contains.
     */
    public Mono<EmoteSet> emoteSet(int emoteSetId) {
        return request(Mono.just("/set/" + emoteSetId), EmoteSet.class);
    }

    /**
     * Gets the information for a specific emote set, including all results it contains.
     *
     * @param emoteSetId the publisher of the numeric FFZ ID for the emote set.
     *
     * @return The information for the emote set and all results it contains.
     */
    public Mono<EmoteSet> emoteSet(Mono<Integer> emoteSetId) {
        return request(emoteSetId.map(id -> "/set/" + id), EmoteSet.class);
    }

    /**
     * Returns the set of global emotes.
     *
     * @return The set of global emotes in a stream as a {@link Flux}.
     */
    public Flux<EmoteSet> globalEmoteSets() {
        return request(Mono.just("/set/global"), GlobalEmotes.class)
                .flatMapMany(model -> {
                    var emoteSets = model.getEmoteSets().values();
                    for (var emoteSet : emoteSets) {
                        var user = model.getUsers().get(Integer.toString(emoteSet.getFfzId()));
                        if (user != null) {
                            emoteSet.initPermittedUsers(user.size()).addAll(user);
                        }
                    }
                    return Flux.fromIterable(emoteSets);
                });
    }

    /**
     * Fetch the information for a user in FFZ, including all referenced badges and emotes.
     *
     * @param userId the user's integer Twitch user ID.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(int userId) {
        return user(userId, true);
    }

    /**
     * Fetch the information for a user in FFZ, including all referenced badges and emotes.
     *
     * @param username the user's username.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(String username) {
        return user(username, true);
    }

    /**
     * Fetch the information for a user in FFZ, including all referenced badges and emotes.
     *
     * @param userId a publisher of the user's Twitch user ID or their username.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(Mono<?> userId) {
        return user(userId, true);
    }

    /**
     * Fetch the information for a user in FFZ.
     *
     * @param userId the Twitch ID of the user.
     * @param includeReferences whether referenced badge and emote information should be included.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(int userId, boolean includeReferences) {
        return request(Mono.just((includeReferences ? "/user/id/" : "/_user/id/") + userId), User.class);
    }

    /**
     * Fetch the information for a user in FFZ.
     *
     * @param username the username of the user.
     * @param includeReferences whether referenced badge and emote information should be included.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(String username, boolean includeReferences) {
        return request(Mono.just((includeReferences ? "/user/" : "/_user/") + username), User.class);
    }

    /**
     * Fetch the information for a user in FFZ.
     *
     * @param userId a publisher of the user's Twitch user ID or their username.
     * @param includeReferences whether referenced badge and emote information should be included.
     *
     * @return The information for the given user.
     */
    public Mono<User> user(Mono<?> userId, boolean includeReferences) {
        var userPublisher = userId.map(id -> {
            if (id instanceof String) {
                return (includeReferences ? "/user/" : "/_user/") + id.toString().toLowerCase(Locale.ROOT);
            } else if (id instanceof Integer) {
                return (includeReferences ? "/user/id/" : "/_user/id/") + id.toString();
            } else {
                throw new IllegalArgumentException("Publisher must yield an integer user ID or string username.");
            }
        });
        return request(userPublisher, User.class);
    }

    /**
     * Perform a get request against the FFZ REST API.
     *
     * @param uri the URI to which to make the request.
     * @param clazz the class of the deserialized response type.
     * @param <T> the type of the deserialized response.
     *
     * @return the deserialized response.
     */
    protected <T> Mono<T> request(Mono<String> uri, Class<T> clazz) {
        return httpClient
                .get()
                .uri(uri)
                .response((response, content) -> {
                    if (response.status().code() >= HttpResponseStatus.BAD_REQUEST.code()) {
                        throw new FfzApiException("Error response " + response.status().code() +
                                " from FFZ API for request.");
                    }
                    return content.aggregate()
                            .asByteArray()
                            .map(input -> {
                                try {
                                    return builder.objectMapper.readValue(input, clazz);
                                } catch (JsonProcessingException e) {
                                    throw new FfzApiException("Incompatible response from FFZ API received.", e);
                                } catch (Exception e) {
                                    throw new FfzApiException("Error reading response from FFZ API.", e);
                                }
                            });
                }).single();
    }

    /**
     * Begin building a new {@link FfzClient}.
     *
     * @return An {@link FfzClientBuilder} used to build a new client with a fluent API.
     */
    public static FfzClientBuilder create() {
        return new FfzClientBuilder();
    }

    /**
     * A builder interface which provides a fluent API for creating a new {@link FfzClient}.
     */
    @ParametersAreNonnullByDefault
    public static final class FfzClientBuilder {
        /**
         * The {@link ConnectionProvider} to use for establishing HTTP connections.
         */
        private ConnectionProvider connectionProvider = HttpResources.get();

        /**
         * The Jackson {@link ObjectMapper} to use to serialize and deserialize JSON data.
         */
        private ObjectMapper objectMapper = new ObjectMapper();

        /**
         * The endpoint of the FFZ REST API.
         */
        private String restEndpoint = "https://api.frankerfacez.com/v1/";

        /**
         * Whether or not to use a secure HTTPS/WSS connection; defaults to <code>true</code>.
         */
        private boolean secure = true;

        /**
         * The SSL provider builder to use to secure the FFZ client connection.
         */
        private Consumer<? super SslProvider.SslContextSpec> sslProviderBuilder;

        /**
         * The {@link MeterRegistry} to which metrics meters will be registered.
         */
        private MeterRegistry meterRegistry = Metrics.globalRegistry;

        /**
         * Creates a new {@link FfzClientBuilder}.
         */
        protected FfzClientBuilder() {
        }

        /**
         * Construct a new {@link FfzClientBuilder} copying an existing one.
         *
         * @param builder the builder to copy.
         */
        protected FfzClientBuilder(final FfzClientBuilder builder) {
            objectMapper = builder.objectMapper;
            connectionProvider = builder.connectionProvider;
            restEndpoint = builder.restEndpoint;
            secure = builder.secure;
            sslProviderBuilder = builder.sslProviderBuilder;
            meterRegistry = builder.meterRegistry;
        }

        /**
         * Set the {@link ConnectionProvider} to use for establishing HTTP connections.
         *
         * @param connectionProvider the {@link ConnectionProvider} to use for establishing HTTP connections.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder connectionProvider(ConnectionProvider connectionProvider) {
            this.connectionProvider = connectionProvider;
            return this;
        }

        /**
         * Set the Jackson {@link ObjectMapper} to use to serialize and deserialize JSON data.
         *
         * @param objectMapper the {@link ObjectMapper} to use.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder objectMapper(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
            return this;
        }

        /**
         * Sets the endpoint where the FFZ REST API is running.
         *
         * @param restEndpoint the endpointwhere the FFZ REST API is running.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder restEndpoint(String restEndpoint) {
            this.restEndpoint = restEndpoint;
            return this;
        }

        /**
         * Enables secure connections for the FFZ client; this is enabled by default.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         *
         * @see FfzClientBuilder#secure(boolean)
         * @see FfzClientBuilder#secure(Consumer)
         */
        public FfzClientBuilder secure() {
            return secure(true);
        }

        /**
         * Whether to use a secure connection to the FFZ API.
         *
         * <p>
         * If no custom SSL provider builder is provided, then this will use the default builder.
         * </p>
         *
         * @param secure whether to secure the connection to the FFZ API.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder secure(boolean secure) {
            this.secure = secure;
            if (!secure) {
                sslProviderBuilder = null;
            }
            return this;
        }

        /**
         * The SSL provider builder to use to secure the FFZ client connections.
         *
         * @param sslProviderBuilder A function which will build the SSL provider.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder secure(Consumer<? super SslProvider.SslContextSpec> sslProviderBuilder) {
            this.sslProviderBuilder = sslProviderBuilder;
            return secure(true);
        }

        /**
         * Sets the {@link MeterRegistry} to which the clients metrics meters will be registered.
         *
         * <p>
         * If not specified this defaults to the global registry.
         * </p>
         *
         * @param meterRegistry the meter registry that will be used.
         *
         * @return A reference to this {@link FfzClientBuilder}.
         */
        public FfzClientBuilder meterRegistry(MeterRegistry meterRegistry) {
            this.meterRegistry = meterRegistry;
            return this;
        }

        /**
         * Builds the {@link FfzClient} and configures the reactive streams for use.
         *
         * @return A new {@link FfzClient}.
         */
        public FfzClient connect() {
            return new FfzClient(this);
        }
    }
}
