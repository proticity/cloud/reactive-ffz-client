/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Represents the data for an FFZ emote.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ParametersAreNonnullByDefault
public class Emote implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A type reference to a sorted map of URL strings.
     */
    private static final TypeReference<TreeMap<Integer, String>> URLS_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * The unique ID of the emote in FFZ.
     */
    private int ffzId;

    /**
     * The name of the emote.
     */
    private String name;

    /**
     * A CSS style to be applied to the displayed emote, if any.
     */
    private String css;

    /**
     * The height of the emote, in pixels.
     */
    private int height;

    /**
     * The width of the emote, in pixels.
     */
    private int width;

    /**
     * Whether this emote has been hidden in the FFZ catalog.
     */
    private boolean hidden;

    /**
     * Whether this emote has been shared publicly to other FFZ users.
     */
    private boolean isPublic;

    /**
     * The extra margins to apply to this emote when displayed, if not default.
     */
    private Integer margins;

    /**
     * The extra spacing offset to apply to this emote when displayed, if not default.
     */
    private Integer offset;

    /**
     * Information on the user who owns the emote.
     */
    private UserReference owner;

    /**
     * A sorted tree map of size multiples with emote images defined to the URL to the image.
     */
    private TreeMap<Integer, String> urls;

    /**
     * Construct an {@link Emote}.
     */
    protected Emote() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Emote emote = (Emote) o;
        return ffzId == emote.ffzId &&
                height == emote.height &&
                width == emote.width &&
                hidden == emote.hidden &&
                isPublic == emote.isPublic &&
                name.equals(emote.name) &&
                Objects.equals(css, emote.css) &&
                Objects.equals(margins, emote.margins) &&
                Objects.equals(offset, emote.offset) &&
                owner.equals(emote.owner) &&
                urls.equals(emote.urls);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(ffzId, name, css, height, width, hidden, isPublic, margins, offset, owner, urls);
    }

    /**
     * Return the URL for the emote image in its base size (1x).
     *
     * @return The URL for the emote image in its base size (1x).
     *
     * @see Emote#getUrl(int)
     */
    @JsonIgnore
    public String getUrl() {
        return getUrl(1);
    }

    /**
     * Return the URL for the emote image for the given size multiplier.
     *
     * <p>
     * This function will provide the best possible image URL for the size requested. Typically FFZ keeps results in the
     * base size (1x), and can also have a 2x and 4x size. When there is no exact match to9 the size requested this
     * method will return the best match, which is defined as the next larger size if one exists, or the next lower size
     * otherwise.
     * </p>
     *
     * @param size The size multiplier for the URL of the image to get.
     *
     * @return A URL to an emote image which is most appropriate to be rendered at the given size multiplier.
     *
     * @see Emote#getUrl()
     */
    public String getUrl(int size) {
        if (size < 1) {
            throw new IllegalArgumentException(
                    "Size is a multiplier for the base image size and must be greater than zero.");
        }
        return "https:" + Objects.requireNonNullElse(urls.ceilingEntry(size), urls.floorEntry(size)).getValue();
    }

    /**
     * Returns the full set of image URLS.
     *
     * @return The full set of image URLS.
     */
    @JsonGetter("urls")
    protected Map<Integer, String> getUrls() {
        return urls;
    }

    /**
     * Returns the unique ID of the emote in FFZ.
     *
     * @return The unique ID of the emote in FFZ.
     */
    public int getFfzId() {
        return ffzId;
    }

    /**
     * Returns the name of the emote (the phrase which for which the emote is substituted in text).
     *
     * @return The name of the emote.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the CSS style to be applied to the displayed emote, if any.
     *
     * @return The CSS style to be applied to the displayed emote, if any.
     */
    public Optional<String> getCss() {
        return Optional.ofNullable(css);
    }

    /**
     * Returns the height of the emote, in pixels.
     *
     * @return The height of the emote, in pixels.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the width of the emote, in pixels.
     *
     * @return The width of the emote, in pixels.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns whether this emote has been hidden in the FFZ catalog.
     *
     * @return Whether this emote has been hidden in the FFZ catalog.
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * Returns whether this emote has been shared publicly to other FFZ users.
     *
     * @return Whether this emote has been shared publicly to other FFZ users.
     */
    public boolean isPublic() {
        return isPublic;
    }

    /**
     * Sets whether this emote is public.
     *
     * @param isPublic whether this emote is public.
     */
    protected void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    /**
     * Returns the extra margins to apply to this emote when displayed, if not default.
     *
     * @return The extra margins to apply to this emote when displayed, if not default.
     */
    public Optional<Integer> getMargins() {
        return Optional.ofNullable(margins);
    }

    /**
     * Returns the extra spacing offset to apply to this emote when displayed, if not default.
     *
     * @return The extra spacing offset to apply to this emote when displayed, if not default.
     */
    public Optional<Integer> getOffset() {
        return Optional.ofNullable(offset);
    }

    /**
     * Returns information on the user who owns the emote.
     *
     * @return Information on the user who owns the emote.
     */
    public UserReference getOwner() {
        return owner;
    }

    /**
     * A JSON deserializer for emotes.
     */
    static class EmoteDeserializer extends StdDeserializer<Emote> {
        private static final long serialVersionUID = 0;

        /**
         * Constructs a default {@link EmoteDeserializer}.
         */
        EmoteDeserializer() {
            this(null);
        }

        /**
         * Constructs a default {@link EmoteDeserializer}.
         */
        EmoteDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Emote deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var emote = new Emote();

            var emoteNode = rootNode.get("emote");
            if (emoteNode == null) {
                emoteNode = rootNode;
            }

            emote.ffzId = emoteNode.get("id").asInt();
            emote.name = emoteNode.get("name").textValue();
            emote.css = emoteNode.path("css").textValue();
            emote.hidden = emoteNode.path("hidden").asBoolean();
            emote.isPublic = emoteNode.path("public").asBoolean();
            emote.height = emoteNode.get("height").asInt();
            emote.width = emoteNode.get("width").asInt();
            emote.margins = (Integer) emoteNode.path("margins").numberValue();
            emote.offset = (Integer) emoteNode.path("offset").numberValue();

            var subParser = emoteNode.get("urls").traverse(jsonParser.getCodec());
            emote.urls = subParser.readValueAs(URLS_TYPE_REFERENCE);

            var ownerNode = emoteNode.get("owner");
            emote.owner = new UserReference();
            emote.owner.ffzId = ownerNode.get("_id").asInt();
            emote.owner.displayName = ownerNode.get("display_name").textValue();
            emote.owner.username = ownerNode.get("name").textValue();

            return emote;
        }
    }
}
