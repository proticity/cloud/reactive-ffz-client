/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.ffz.client;

import java.util.function.Consumer;
import javax.annotation.ParametersAreNonnullByDefault;

import reactor.core.Disposable;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

/**
 * A handle to emote search results which begins streaming results as a hot source after connection.
 *
 * <p>
 * This handle will not return results until {@link ConnectableEmoteResults#connect()} has been called, allowing for
 * independent subscriptions to be made to both the emote results themselves and the stream of current total matching
 * emotes. To simplify connecting to a single stream {@link ConnectableEmoteResults#autoConnect()} can be used.
 * </p>
 *
 * @see EmoteResults
 */
@ParametersAreNonnullByDefault
public class ConnectableEmoteResults {
    /**
     * The stream of the current total number of matching results in the response.
     */
    Flux<Integer> totalResults;

    /**
     * The stream of results from the response.
     */
    Flux<Emote> results;

    /**
     * The source of the output stream.
     */
    ConnectableFlux<EmoteSearchPage> source;

    /**
     * Creates a new {@link ConnectableEmoteResults} handle.
     */
    ConnectableEmoteResults() {
    }

    /**
     * Returns an auto-connecting handle which begins streaming as soon as there are any subscribers to either
     * results or total counts.
     *
     * @return An auto-connecting results handle.
     */
    public EmoteResults autoConnect() {
        return new EmoteResults(source.autoConnect());
    }

    /**
     * Connect the source stream and begin streaming results.
     *
     * @return A {@link Disposable} for disconnecting.
     */
    public Disposable connect() {
        return source.connect();
    }

    /**
     * Connect the source stream and begin streaming results.
     *
     * @param cancelSupport A callback which can be used to handle the disconnection from the source.
     */
    public void connect(Consumer<? super Disposable> cancelSupport) {
        source.connect(cancelSupport);
    }

    /**
     * Returns a {@link Flux} of total emote counts in the result.
     *
     * <p>
     * The total emote count is returned with each page of the results in an emote search. For the most accurate
     * results the number of results streamed in the results can be counted. For results which may not match the number
     * returned but which is most up-to-date with regard to what is present in FFZ at the end of the results, use
     * <code>totalResults().last()</code> to get a {@link reactor.core.publisher.Mono} for the number included in the
     * final page. To get this count quickly you can return the number of results at the time the first page is found
     * with <code>totalResults().first()</code>.
     * </p>
     *
     * @return A stream of the known number of matching results at the time each page of results is returned.
     */
    public Flux<Integer> totalResults() {
        if (totalResults == null) {
            totalResults = source.map(EmoteSearchPage::getTotalEmoticons);
        }
        return totalResults;
    }

    /**
     * The stream of results from the response.
     *
     * @return The stream of results from the response.
     */
    public Flux<Emote> results() {
        if (results == null) {
            results = source.flatMap(page -> Flux.fromIterable(page.getEmoticons()));
        }
        return results;
    }
}
