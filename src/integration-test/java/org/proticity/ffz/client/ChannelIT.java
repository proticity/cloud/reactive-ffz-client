package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import java.util.UUID;

public class ChannelIT {
    private static FfzClient client;

    @BeforeAll
    public static void setup() {
        client = FfzClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testRoomByName() {
        // Use capitalization here to verify it still works; FFZ expects all lowercase as in Twitch reference usernames.
        var channel = client.channel("CharmedBaryon").block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertEquals("charmedbaryon", channel.getName());
        var emoteSet = channel.getEmoteSets().get(235637);
        Assertions.assertNotNull(emoteSet);
        Assertions.assertTrue(emoteSet.isPresent());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertNotNull(channel.getModeratorBadge().get().getUrl());
        Assertions.assertThrows(IllegalArgumentException.class, () -> channel.getModeratorBadge().get().getUrl(0));
    }

    @Test
    public void testRoomById() {
        // Use capitalization here to verify it still works; FFZ expects all lowercase as in Twitch reference usernames.
        var channel = client.channel(70902416).block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertEquals("charmedbaryon", channel.getName());
        var emoteSet = channel.getEmoteSets().get(235637);
        Assertions.assertNotNull(emoteSet);
        Assertions.assertTrue(emoteSet.isPresent());
        Assertions.assertFalse(channel.isGroup());
        Assertions.assertTrue(channel.getCss().isEmpty());
        Assertions.assertTrue(channel.getModeratorBadge().isPresent());
        Assertions.assertNotNull(channel.getModeratorBadge().get().getUrl());
        Assertions.assertThrows(IllegalArgumentException.class, () -> channel.getModeratorBadge().get().getUrl(0));
    }

    @Test
    public void testRoomNoEmoteSets() {
        var channel = client.channel("charmedbaryon", false).block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("CharmedBaryon", channel.getDisplayName());
        Assertions.assertEquals("charmedbaryon", channel.getName());
        var emoteSet = channel.getEmoteSets().get(235637);
        Assertions.assertNotNull(emoteSet);
        Assertions.assertTrue(emoteSet.isEmpty());
    }

    @Test
    public void testRoomNotFound() {
        Assertions.assertThrows(FfzApiException.class, () -> client.channel(UUID.randomUUID().toString()).block());
    }
}
