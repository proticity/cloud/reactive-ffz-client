package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import java.io.IOException;
import java.util.UUID;

public class UserIT {
    private static FfzClient client;

    @BeforeAll
    public static void setup() {
        client = FfzClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testUserByName() {
        var user = client.user("sirstendec").block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertTrue(user.isDonor());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("developer", user.getBadges().get(1).get().getName());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isPresent());
        Assertions.assertNotNull(user.getAvatar());
    }

    @Test
    public void testUserById() throws IOException {
        var user = client.user(49399878).block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertTrue(user.isDonor());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertEquals("developer", user.getBadges().get(1).get().getName());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isPresent());
        Assertions.assertNotNull(user.getAvatar());
    }

    @Test
    public void testUserNoReferences() {
        var user = client.user("sirstendec", false).block();
        Assertions.assertNotNull(user);
        Assertions.assertEquals(49399878, user.getTwitchId());
        Assertions.assertEquals(1, user.getFfzId());
        Assertions.assertTrue(user.isDonor());
        Assertions.assertEquals("SirStendec", user.getDisplayName());
        Assertions.assertEquals("sirstendec", user.getUsername());
        Assertions.assertTrue(user.getBadges().get(1).isEmpty());
        Assertions.assertTrue(user.getEmoteSets().get(4330).isEmpty());
    }

    @Test
    public void testUserNotFound() {
        Assertions.assertThrows(FfzApiException.class, () -> client.user(UUID.randomUUID().toString()).block());
    }
}
