package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class EmoteIT {
    private static FfzClient client;

    @BeforeAll
    public static void setup() {
        client = FfzClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testEmoteSearch() {
        var results = client.emotes(EmoteSearch.forMax(750));
        var total = results.autoConnect().results().reduce(0, (i, emote) -> i + 1).block();
        Assertions.assertNotNull(total);
        Assertions.assertEquals(750, total.intValue());
    }

    @Test
    public void testEmoteSearchManual() {
        var results = client.emotes(EmoteSearch.forMax(750));

        // Test via manual connect.
        new Thread(results::connect).start();

        var total = results.results().reduce(0, (i, emote) -> i + 1).block();
        Assertions.assertNotNull(total);
        Assertions.assertEquals(750, total.intValue());
    }

    @Test
    public void testEmoteSearchTotalsManual() throws InterruptedException {
        var results = client.emotes(EmoteSearch.forMax(750));
        var latch = new CountDownLatch(1);

        // Test via manual connect.
        new Thread(() -> {
            results.connect(connection -> latch.countDown());
        }).start();

        var totalPages = results.totalResults().reduce(0, (i, total) -> {
            Assertions.assertNotNull(total);
            Assertions.assertTrue(total > 0);
            return i + 1;
        }).block();
        Assertions.assertEquals(4, totalPages.intValue());

        Assertions.assertTrue(latch.await(100, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testEmoteSearchTotals() {
        var results = client.emotes(EmoteSearch.forMax(750));
        var totalPages = results.autoConnect().totalResults().reduce(0, (i, total) -> {
            Assertions.assertNotNull(total);
            Assertions.assertTrue(total > 0);
            return i + 1;
        }).block();
        Assertions.assertEquals(4, totalPages.intValue());
    }

    @Test
    public void testEmoteSearchAligned() {
        var results = client.emotes(EmoteSearch.forMax(200).sortOrder(SortOrder.NAME_DESC));
        var total = results.autoConnect().results().reduce(0, (i, emote) -> i + 1).block();
        Assertions.assertNotNull(total);
        Assertions.assertEquals(200, total.intValue());
    }

    @Test
    public void testEmoteSearchPageTimeout() throws Exception {
        var emotes = client.emotes(EmoteSearch.forMax(200).sortOrder(SortOrder.UPDATED_ASC)
                .pageTimeout(Duration.ofMillis(1)));
        Assertions.assertThrows(FfzApiException.class, () -> emotes.autoConnect().results().blockLast());
    }

    @Test
    public void testEmoteSearchByPage() {
        var resultsFirst = client.emotes(EmoteSearch.forPage(50, 1));
        var resultsSecond = client.emotes(EmoteSearch.forPage(50, 2));

        var firstEmote = resultsFirst.autoConnect().results().blockFirst();
        var firstPageTwoEmote = resultsSecond.autoConnect().results().blockFirst();
        Assertions.assertNotNull(firstEmote);
        Assertions.assertNotNull(firstPageTwoEmote);
        Assertions.assertNotEquals(firstEmote.getName(), firstPageTwoEmote.getName());
    }

    @Test
    public void testEmoteById() {
        var emote = client.emote(239688).block();
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("baryonMaiq", emote.getName());
        Assertions.assertEquals("https://cdn.frankerfacez.com/c2bf41d8c31885cb64c92008f3e49a0c.png",
                emote.getUrl(1));
        Assertions.assertEquals("https://cdn.frankerfacez.com/b4d79602567c58cc3387b3fb8cf73dc0.png",
                emote.getUrl(2));
        Assertions.assertEquals("https://cdn.frankerfacez.com/1b14571f472f3ec789ac46ceae104e5e.png",
                emote.getUrl(3));
        Assertions.assertEquals("https://cdn.frankerfacez.com/1b14571f472f3ec789ac46ceae104e5e.png",
                emote.getUrl(4));
        Assertions.assertEquals("https://cdn.frankerfacez.com/1b14571f472f3ec789ac46ceae104e5e.png",
                emote.getUrl(5));
    }

    @Test
    public void testEmoteSet() {
        var emoteSet = client.emoteSet(4330).block();
        Assertions.assertNotNull(emoteSet);
        Assertions.assertEquals(4330, emoteSet.getFfzId());
        Assertions.assertEquals(": Sten's Cheaty Emotes", emoteSet.getTitle());
        Assertions.assertEquals(EmoteSetType.GLOBAL, emoteSet.getType());
        Assertions.assertTrue(emoteSet.getPermittedUsers().isPresent());
        Assertions.assertTrue(emoteSet.getPermittedUsers().get().contains("sirstendec"));

        var andKnuckles = emoteSet.getEmotes().get("AndKnuckles");
        Assertions.assertNotNull(andKnuckles);
        Assertions.assertEquals("AndKnuckles", andKnuckles.getName());

        var oBoy = emoteSet.getEmotes().get("OBOY");
        Assertions.assertNotNull(oBoy);
        Assertions.assertEquals("OBOY", oBoy.getName());
    }

    @Test
    public void testEmoteSetNotFound() {
        Assertions.assertThrows(FfzApiException.class, () -> client.emoteSet(Integer.MAX_VALUE).block());
    }

    @Test
    public void testGlobalEmoteSets() {
        var emoteSets = client.globalEmoteSets().collectMap(EmoteSet::getFfzId).block();
        Assertions.assertNotNull(emoteSets);

        var globalSet = emoteSets.get(3);
        Assertions.assertTrue(globalSet.getPermittedUsers().isEmpty());
        Assertions.assertEquals("Global Emotes", globalSet.getTitle());
        Assertions.assertEquals(3, globalSet.getFfzId());
        Assertions.assertTrue(globalSet.getEmotes().size() > 0);
        Assertions.assertEquals(EmoteSetType.GLOBAL, globalSet.getType());

        var personalEmotes = emoteSets.get(4330);
        Assertions.assertTrue(personalEmotes.getPermittedUsers().isPresent());
        Assertions.assertTrue(personalEmotes.getPermittedUsers().get().contains("sirstendec"));
        Assertions.assertEquals(4330, personalEmotes.getFfzId());
        Assertions.assertEquals(": Sten's Cheaty Emotes", personalEmotes.getTitle());
        Assertions.assertTrue(personalEmotes.getEmotes().size() > 0);
        Assertions.assertEquals(EmoteSetType.GLOBAL, personalEmotes.getType());
    }
}
