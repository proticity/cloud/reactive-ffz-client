package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.handler.ssl.SslContextBuilder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import reactor.netty.http.HttpResources;

import javax.net.ssl.SSLException;

public class FfzClientIT {
    private static FfzClient client;

    @BeforeAll
    public static void setup() {
        client = FfzClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .secure(builder -> {
                    // Ensure the SSL builder works.
                    try {
                        builder.sslContext(SslContextBuilder.forClient().build());
                    } catch (SSLException e) {
                        throw new RuntimeException(e);
                    }
                })
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }
}
