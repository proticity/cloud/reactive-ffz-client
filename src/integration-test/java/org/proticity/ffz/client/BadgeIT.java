package org.proticity.ffz.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

public class BadgeIT {
    private static FfzClient client;

    @BeforeAll
    public static void setup() {
        client = FfzClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testBadges() {
        var badgeInfo = client.badges().block();
        Assertions.assertNotNull(badgeInfo);
        Assertions.assertTrue(badgeInfo.getBadges().size() > 0);
        Assertions.assertTrue(badgeInfo.getBadgeAssignments().isPresent());
        for (var assignmentEntry : badgeInfo.getBadgeAssignments().get().entrySet()) {
            for (var badgeEntry : assignmentEntry.getValue().entrySet()) {
                Assertions.assertSame(badgeEntry.getValue(), badgeInfo.getBadges().get(badgeEntry.getKey()));
            }
        }
        var badge = badgeInfo.getBadges().get(1);
        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertNotNull(badge.getUrl());
    }

    @Test
    public void testBadgesNoAssignments() {
        var badgeInfo = client.badges(false).block();
        Assertions.assertNotNull(badgeInfo);
        Assertions.assertTrue(badgeInfo.getBadges().size() > 0);
        Assertions.assertTrue(badgeInfo.getBadgeAssignments().isEmpty());
    }

    @Test
    public void testBadgeById() {
        var badge = client.badge(1).block();
        Assertions.assertNotNull(badge);

        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getAssignments().isPresent());
        Assertions.assertTrue(badge.getAssignments().get().size() > 0);

        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/1", badge.getUrl(1));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/2", badge.getUrl(2));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(3));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(4));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> badge.getUrl(0));
    }

    @Test
    public void testBadgeByName() {
        var badge = client.badge("developer").block();
        Assertions.assertNotNull(badge);

        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getAssignments().isPresent());
        Assertions.assertTrue(badge.getAssignments().get().size() > 0);

        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/1", badge.getUrl(1));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/2", badge.getUrl(2));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(3));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(4));
        Assertions.assertEquals("https://cdn.frankerfacez.com/badge/1/4", badge.getUrl(5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> badge.getUrl(0));
    }

    @Test
    public void testBadgeNoAssignments() {
        var badge = client.badge(1, false).block();
        Assertions.assertNotNull(badge);
        Assertions.assertEquals(1, badge.getFfzId());
        Assertions.assertEquals("developer", badge.getName());
        Assertions.assertEquals("FFZ Developer", badge.getTitle());
        Assertions.assertTrue(badge.getAssignments().isEmpty());
    }
}
